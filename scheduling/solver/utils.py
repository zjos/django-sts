from scheduling.solver.course import Course
from scheduling.solver.student import Student

class Parser():
    """
    Parser of the inputs
    """

    def parse_tag(self, tag):
        if tag is 1:
            return "Lecture"
        elif tag is 2:
            return "Exercise"
        else:
            return None

    def get_courses_list(self, courses):
        result = []
        for course in courses:
                result.append(course["course"])
        return result

    def parse_students_dict(self, students_dict):
        result = []
        for student in students_dict:
            v = student["student"]
            code = v[0]["code"]
            courses_list = self.get_courses_list(v[1]["course_list"])
            new_student = Student(code, courses_list)
            result.append(new_student)
        return result

    def get_dayFromCode(self, code):
        if code[0] is "L":
            return 1
        elif code[0] is "M":
            return 2
        elif code[0] is "W":
            return 3
        elif code[0] is "J":
            return 4
        elif code[0] is "V":
            return 5
        elif code[0] is "S":
            return 6
        else:
            return None

    def get_shiftFromCode(self, code):
        if code[1:]=="12":
            return 1
        if code[1:]=="34":
            return 2
        if code[1:]=="56":
            return 3
        if code[1:]=="78":
            return 4
        else:
            return None

    def parse_code(self, code):
        day = self.get_dayFromCode(code)
        shift = self.get_shiftFromCode(code)
        return day, shift

    def parse_demiSemester(self, demi_semester):
        if demi_semester is 1:
            return "Qa"
        elif demi_semester is 2:
            return "Qb"
        else :
            return None

    """
    parse course in a dictionary
    """
    def parse_courses_dict(self, students_dict):
        result = []
        for course in students_dict:
            element = course["course"]
            sigle = element[0].get("sigle")
            n_CM_Qa = element[1].get("n_CM_Qa")
            slots_CM_Qa = self.get_slots_list(element[2].get("slots_CM_Qa"))
            n_CM_Qb = element[3].get("n_CM_Qb")
            slots_CM_Qb = self.get_slots_list(element[4].get("slots_CM_Qb"))
            n_EX_Qa = element[5].get("n_EX_Qa")
            slots_EX_Qa = self.get_slots_list(element[6].get("slots_EX_Qa"))
            n_EX_Qb = element[7].get("n_EX_Qb")
            slots_EX_Qb = self.get_slots_list(element[8].get("slots_EX_Qb"))
            new_course = Course(sigle, n_CM_Qa, slots_CM_Qa, n_CM_Qb, slots_CM_Qb, n_EX_Qa, slots_EX_Qa, n_EX_Qb, slots_EX_Qb)
            result.append(new_course)
        return result

    def get_slots_list(self, slots):
        result = []
        if not slots:
            return result
        for dict in slots:
            slot = dict.get("slot")
            result.append(slot)
        return result

    def get_lecture_days(self, split_line, index):
        numLecDays = int(split_line[index])
        lecDays = []
        for i in range(1,numLecDays+1):
            lecDays.append(split_line[index+i])
        return numLecDays, lecDays

    def get_lab_days(self, split_line, numLecDays, numLabDays):
        labDays = []
        for i in range(1,numLabDays+1):
            labDays.append(split_line[11+numLecDays+i])
        return labDays

    def parse_account(self, account):
        if account is 1:
            return "staff"
        else :
            return None

    def get_day_code(self, day):
        if day is 1:
            return "L"
        elif day is 2:
            return "M"
        elif day is 3:
            return "W"
        elif day is 4:
            return "J"
        elif day is 5:
            return "V"
        elif day is 6:
            return "S"
        elif day is 7:
            return "D"
        else:
            return None

    def get_shift_code(self, shift):
        if shift is 1:
            return "12"
        elif shift is 2:
            return "34"
        elif shift is 3:
            return "56"
        elif shift is 4:
            return "78"
        else :
            return None

    def parse_day(self, day):
        if day is 1:
            return "Mon"
        elif day is 2:
            return "Tue"
        elif day is 3:
            return "Wed"
        elif day is 4:
            return "Thu"
        elif day is 5:
            return "Fri"
        elif day is 6:
            return "Sat"
        elif day is 7:
            return "Sun"
        else:
            return None

    def parse_shift(self, shift):
        if shift is 1:
            return "08:30-10:30"
        elif shift is 2:
            return "10:45-12:45"
        elif shift is 3:
            return "14:00-16:00"
        elif shift is 4:
            return "16:15-18:15"
        elif shift is 5:
            return "18:30-20:30"
        else:
            return None