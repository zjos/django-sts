__author__ = 'admin'
from scheduling.models import *

class Stat():
    """
    This class will contain all the information needed to analyse the results
    """
    def __init__(self, students, courses):
        self.n_students = len(students)
        self.students = students
        self.n_courses = len(courses)
        self.courses = courses
        self.no_possibility_qa = No_possibility(self.student_with_no_possibility_QA(), self.n_students)
        self.no_possibility_qb = No_possibility(self.student_with_no_possibility_QB(), self.n_students)
        #self.multiple_possibility_qb = Multiple_possibility(self.student_with_multiple_possibility_QB(), self.n_students)

    def init_studentPossibility(self, students):
        result = []
        for student in students:
            result.append(Student_possibilities(student))
        return result

    """
    Get students with no possibility for QA
    """
    def student_with_no_possibility_QA(self):
        result = []
        for student in self.students:
            student_courses = StudentCourse.objects.filter(student=student)
            courses = []
            for object in student_courses:
                if Slot_EX_QA.objects.filter(course=object.course).exists():
                    if not PossibilityQA.objects.filter(Q(student=student) & Q(course=object.course)).exists():
                        courses.append(object.course)
            if courses:
                student_dict = {'student':student,
                                'courses': courses}
                result.append(Student_possibilities(student_dict))
        return result

    """
    Get students with no possibility for QB
    """
    def student_with_no_possibility_QB(self):
        result = []
        for student in self.students:
            student_courses = StudentCourse.objects.filter(student=student)
            courses = []
            for object in student_courses:
                if Slot_EX_QB.objects.filter(course=object.course).exists():
                    if not PossibilityQB.objects.filter(Q(student=student) & Q(course=object.course)).exists():
                        courses.append(object.course)
            if courses:
                student_dict = {'student':student,
                                'courses': courses}
                result.append(Student_possibilities(student_dict))
        return result

    """
    Get students with no possibility for QA
    """
    def student_with_multiple_possibility_QA(self):
        result = []
        for student in self.students:
            student_courses = StudentCourse.objects.filter(student=student)
            courses = []
            for object in student_courses:
                if Slot_EX_QA.objects.filter(course=object.course).exists():
                    if PossibilityQA.objects.filter(Q(student=student) & Q(course=object.course)).count() > 1:
                        courses.append(object.course)
            if courses:
                student_dict = {'student':student,
                                'courses': courses}
                result.append(Student_possibilities(student_dict))
        return result

    """
    Get students with no possibility for QB
    """
    def student_with_multiple_possibility_QB(self):
        result = []
        for student in self.students:
            student_courses = StudentCourse.objects.filter(student=student)
            courses = []
            for object in student_courses:
                if Slot_EX_QB.objects.filter(course=object.course).exists():
                    if PossibilityQB.objects.filter(Q(student=student) & Q(course=object.course)).count() > 1:
                        courses.append(object.course)
            if courses:
                student_dict = {'student':student,
                                'courses': courses}
                result.append(Student_possibilities(student_dict))
        return result

class Lab_no_student():
    """
    This class represente all the labs where there is not students
    """
    def __init__(self, labs, total_labs):
        self.numb_labs = len(labs)
        self.labs = labs
        self.percentage = str(float("{0:.2f}".format((self.numb_labs/total_labs)*100)))+"%"

class No_possibility():
    """
    This class represent students with no possibilities
    """
    def __init__(self, students, total_students):
        self.students = students
        self.numb_students = len(students)
        self.percentage = self.get_percentage(total_students)

    # get percentage
    def get_percentage(self, total_students):
        if total_students > 0 :
            return str(float("{0:.2f}".format((self.numb_students/total_students)*100)))+"%"
        else:
            return str(float("{0:.2f}".format((0)*100)))+"%"

class Multiple_possibility():

    def __init__(self, students, total_students):
        self.students = students
        self.numb_students = len(students)
        self.percentage = self.get_percentage(total_students)

    def get_percentage(self, total_students):
        if total_students > 0:
            return str(float("{0:.2f}".format((self.numb_students/total_students)*100)))+"%"
        else:
            return str(float("{0:.2f}".format((0)*100)))+"%"


class Student_possibilities():
    """
    This class represent students with multiple possibilities
    """
    def __init__(self, student):
        self.student = student["student"]
        self.n_courses = len(student["courses"])
        self.courses = student["courses"]

    def courses_to_string(self):
        result = ""
        for course in self.courses:
            result += str(course) + " "
        return result


class ScheduleStat():
    """
    This class represent the stat of a schedule
    """
    def __init__(self, schedule):
        self.schedule = schedule
        self.labs_no_student, self.total_labs = self.lab_session_with_no_students()
        self.n_labs_no_student = len(self.labs_no_student)
        self.percentage = self.get_percentage()

    def get_percentage(self):
        if self.total_labs > 0:
            return str(float("{0:.2f}".format((self.n_labs_no_student/self.total_labs)*100)))+"%"
        else:
            return str(float("{0:.2f}".format((0)*100)))+"%"

    """
    Get lab session with no students for a schedule
    """
    def lab_session_with_no_students(self):
        result = []
        numb = 0
        courses = Course.objects.all()
        for course in courses:
            if self.schedule.demi_semester == 1:
                labs = Slot_EX_QA.objects.filter(course=course)
                for lab in labs:
                    numb += 1
                    if not StudentCourseExQa.objects.filter(Q(course=course) & Q(ex_qa_slot=lab)).exists():
                        result.append(lab)
            else:
                labs = Slot_EX_QB.objects.filter(course=course)
                for lab in labs:
                    numb += 1
                    if not StudentCourseExQb.objects.filter(Q(course=course) & Q(ex_qb_slot=lab)).exists():
                        result.append(lab)

        return result, numb
