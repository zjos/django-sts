from scheduling.enum import SHIFTS, DAYS
class Course():
    """
    This class represent an course
    """
    def __init__(self, sigle, n_CM_Qa, slots_CM_Qa, n_CM_Qb, slots_CM_Qb, n_EX_Qa, slots_EX_Qa, n_EX_Qb, slots_EX_Qb):
        self.sigle = sigle
        self.n_CM_Qa = n_CM_Qa
        self.slots_CM_Qa = slots_CM_Qa
        self.n_CM_Qb = n_CM_Qb
        self.slots_CM_Qb = slots_CM_Qb
        self.n_EX_Qa = n_EX_Qa
        self.slots_EX_Qa = slots_EX_Qa
        self.n_EX_Qb = n_EX_Qb
        self.slots_EX_Qb = slots_EX_Qb


    def __str__(self):
        result = ""
        result += "sigle : " + self.sigle + "\n"
        result += "number of CM QA : " + str(self.n_CM_Qa) + "\n"
        result += "slot CM QA : " + str(self.slots_CM_Qa) + "\n"
        result += "number of CM QB : " + str(self.n_CM_Qb) + "\n"
        result += "slot CM QB : " + str(self.slots_CM_Qb) + "\n"
        result += "number of EX QA : " + str(self.n_EX_Qa) + "\n"
        result += "slot EX QA : " + str(self.slots_EX_Qa) + "\n"
        result += "number of EX QB : " + str(self.n_EX_Qb) + "\n"
        result += "slot EX QB : " + str(self.slots_EX_Qb) + "\n"

        return result