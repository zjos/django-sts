
class Student():
    """
    This class represent a student
    """
    def __init__(self, code, courses_list):
        self.code = code
        self.courses_list = courses_list

    def __str__(self):
        result = ""
        result += str(self.code) + "\n"
        for course in self.courses_list:
            result += "\t"+course+"\n"
        return result


