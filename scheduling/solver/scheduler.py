from scheduling.solver.course import Course
from scheduling.models import Slot_CM_QA, Slot_CM_QB, Slot_EX_QA, Slot_EX_QB
from scheduling.enum import DAYS

days = ["Monday","Tuesday","Wednesday","Thursday","Friday"]
times = ["800","830","900","930","1000","1030","1100","1130","1200","1230","1300","1330","1400","1430","1500","1530","1600","1630","1700","1730","1800","1830","1900","1930","2000","2030","2100","2130","2200"]

class Scheduler():
    """
    This class is the builder of a schedule by creating all his events
    """

    # configuration
    def __init__(self, schedule, courses):
        self.schedule = schedule
        self.num_courses = courses.count()
        self.courses = courses

    def __str__(self):
        result = ""
        result += "schedule_name="+self.schedule.name+", "
        result += "schedule_description="+self.schedule.description+", "
        for course in self.courses :
            result += "course="+ str(course)+" "
        return result

