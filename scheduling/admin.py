# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from scheduling.forms import *
from scheduling.models import *




class RoomAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'capacity')
    search_fields = ['name', 'address', 'capacity']

# (TAB_NAME, TAB_TITLE)
suit_form_tabs = (('create_student', 'New student'), ('create_instructor', 'New instructor'))


class UserProfileAdmin(UserAdmin):
    add_form = UserProfileCreationForm
    form = UserProfileChangeForm
    model = UserProfile
    list_display = ['email', 'username', 'account_type']


admin.site.site_header = 'Scheduling timetable administration'

# Register your models here.
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Event)
admin.site.register(Schedule)
admin.site.register(Course)
admin.site.register(Student)
admin.site.register(Slot)
admin.site.register(PossibilityQB)
admin.site.register(PossibilityQA)