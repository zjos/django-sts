from enum import Enum
ACCOUNT_TYPE = (
    (1, ('staff')),
)
DAYS = (
    #(0, ('No day')),
    (1, ('Monday')),
    (2, ('Tuesday')),
    (3, ('Wednesday')),
    (4, ('Thursday')),
    (5, ('Friday')),
    #(6, ('Saturday')),
    #(7, ('Sunday')),
)

SHIFTS = (
    #(0, ('No shift')),
    (1, ('08:30-10:30')),
    (2, ('10:45-12:45')),
    (3, ('14:00-16:00')),
    (4, ('16:15-18:15')),
    (5, ('18:30-20:30')),
)
TAG = (
    (1, ('Lecture')),
    (2, ('Exercise')),
)

DEMISEMESTER = (
    (1, ('First demi semester Qa')),
    (2, ('Second demi semester Qb'))
)
NUM_DAYS = (
    (1, ('One day')),
    (2, ('Two days')),
    (3, ('Three days')),
    (4, ('Four days')),
    (5, ('Five days')),
    (6, ('Six days')),
    (7, ('Seven days'))
)
NUM_SHIFTS = (
    (1, ('One shifts')),
    (2, ('Two shifts')),
    (3, ('Three shifts')),
    (4, ('Four shifts')),
    (5, ('Five shifts')),
    (6, ('Six shifts')),
)