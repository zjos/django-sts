from django.urls import include, path
from scheduling.views import calendar_views, account_views, registration_views, solver_views, study_views
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url

urlpatterns = [
    # Calendar
    path('calendar/display/<int:pk>/', calendar_views.calendar_display, name='calendar_display'),
    path('calendar/events', calendar_views.EventListView.as_view(), name='events_list'),
    path('calendar/event/<int:pk>/', calendar_views.EventDetailView.as_view(), name='event_details'),
    path('calendar/events/new', calendar_views.EventCreateView.as_view(), name='new_event'),
    path('calendar/events/edit/<int:pk>', calendar_views.EventUpdateView.as_view(), name='edit_event'),
    path('calendar/event/<int:pk>/delete/', calendar_views.EventDeleteView.as_view(), name='delete_event'),

    # Lab session
    path('solver/schedules/labs/<int:pk>', solver_views.labs, name="labs"),

    # Genetic computation
    path('solver/schedules/labs/genetic/computation/<int:pk>', solver_views.ga_computation, name="ga_computation"),

    # Solver
    path('solver/schedules', solver_views.ScheduleListView.as_view(), name="schedules_list"),
    path('solver/schedule/<int:pk>/', solver_views.ScheduleDetailView.as_view(), name="schedule_details"),
    path('solver/schedules/new', solver_views.ScheduleCreateView.as_view(), name="new_schedule"),
    path('solver/schedules/edit/<int:pk>/', solver_views.ScheduleUpdateView.as_view(), name="edit_schedule"),
    path('solver/schedule/<int:pk>/delete/', solver_views.ScheduleDeleteView.as_view(), name="delete_schedule"),
    path('solver/schedule/events/<int:pk>', solver_views.EventsCreateView.as_view(), name="schedule_new_events"),
    path('solver/schedules/gen/<int:pk>', solver_views.generate_event, name="gen_event"),
    path('solver/schedules/calendar/<int:pk>', solver_views.scheduleCalendar, name="schedule_calendar"),
    path('solver/possibility/', solver_views.possibilities, name="all_possibilities"),
    path('solver/possibility/computation', solver_views.studentPossibilities, name="student_possibilities"),
    path('solver/schedules/lab_groups/<int:pk>', solver_views.compute_lab_groups, name="compute_lab_groups"),
    path('solver/stat', solver_views.studentStat, name="student_stat"),


    # Account path
    path('accounts/profile/', account_views.profile, name="profile"),
    path('accounts/profile/edit_profile', account_views.edit_profile, name="edit_profile"),
    path('accounts/profile/change_password', account_views.change_password, name="change_password"),
    # Account profile
    path('accounts/profile/<int:pk>', account_views.UserAccountDetailView.as_view(), name="account_profile_details"),
    path('accounts/profile/edit/<int:pk>', account_views.UserAccountUpdateView.as_view(), name="edit_account_profile"),

    # Study
    # Slot
    path('study/slot/gen', study_views.genSlots, name="gen_slots"),
    path('study/slots', study_views.SlotListView.as_view(), name="slots_list"),
    path('study/slot/<int:pk>', study_views.SlotDetailView.as_view(), name="slot_details"),
    path('study/slot/new', study_views.SlotCreateView.as_view(), name="new_slot"),
    path('study/slot/edit/<int:pk>', study_views.SlotUpdateView.as_view(), name="edit_slot"),
    path('study/slot/<int:pk>/delete', study_views.SlotDeleteView.as_view(), name="delete_slot"),
    # Lab
    path('study/lab/QA/<int:pk>/schedule/<int:pk_2>/', study_views.Lab_QA_DetailView.as_view(), name="lab_qa_details"),
    path('study/lab/QB/<int:pk>/schedule/<int:pk_2>/', study_views.Lab_QB_DetailView.as_view(), name="lab_qb_details"),
    path('study/lab/new', study_views.LabCreateView.as_view(), name="new_lab"),
    path('study/lab/QA/edit/<int:pk>/schedule/<int:pk_2>', study_views.Lab_QA_UpdateView.as_view(), name="edit_lab_qa"),
    path('study/lab/QB/edit/<int:pk>/schedule/<int:pk_2>', study_views.Lab_QB_UpdateView.as_view(), name="edit_lab_qb"),
    path('study/lab/QA/<int:pk>/delete/<int:pk_2>', study_views.Lab_QA_DeleteView.as_view(), name="delete_lab_qa"),
    path('study/lab/QB/<int:pk>/delete/<int:pk_2>', study_views.Lab_QB_DeleteView.as_view(), name="delete_lab_qb"),
    # Course
    path('study/import_course', study_views.importCourses, name="import_courses"),
    path('study/courses', study_views.CourseListView.as_view(), name="courses_list"),
    path('study/course/<int:pk>', study_views.CourseDetailView.as_view(), name="course_details"),
    path('study/course/<int:pk_1>/schedule/<int:pk_2>', study_views.courseSchedule, name="course_schedule"),
    path('study/course/lab/students/<int:pk_1>/schedule/<int:pk_2>/course/<int:pk_3>', study_views.lab_students, name="lab_students_list"),
    path('study/courses/new', study_views.CourseCreateView.as_view(), name="new_course"),
    path('study/courses/edit/<int:pk>', study_views.CourseUpdateView.as_view(), name="edit_course"),
    path('study/course/<int:pk>/delete', study_views.CourseDeleteView.as_view(), name="delete_course"),
    # Student
    path('study/import_student', study_views.importStudents, name="import_students"),
    path('study/students', study_views.StudentListView.as_view(), name="students_list"),
    path('study/student/<int:pk>', study_views.StudentDetailView.as_view(), name="student_details"),
    path('study/student/<int:pk_1>/schedule/<int:pk_2>', study_views.studentSchedule, name="student_schedule"),
    path('study/student/<int:pk_1>/schedule/<int:pk_2>/course/<int:pk_3>', study_views.edit_lab_session, name="edit_student_course_lab"),
    path('study/students/new', study_views.StudentCreateView.as_view(), name="new_student"),
    path('study/students/edit/<int:pk>', study_views.StudentUpdateView.as_view(), name="edit_student"),
    path('study/student/<int:pk>/delete', study_views.StudentDeleteView.as_view(), name="delete_student"),
    path('study/schedule/course/labQa/<int:pk>', study_views.Move_QA_UpdateView.as_view(), name="move_student_qa"),
    path('study/schedule/course/labQb/<int:pk>', study_views.Move_QB_UpdateView.as_view(), name="move_student_qb"),

    # Registration path
    path('accounts/signup/', registration_views.signup.as_view(), name='signup'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)