__author__ = 'admin'

from django.test import TestCase
from scheduling.models import *

# Create your tests here.

class UserProfileModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        UserProfile.objects.create(first_name='Big', last_name='Bob', account_type=1)

    def test_first_name_label(self):
        user = UserProfile.objects.get(id=1)
        field_label = user._meta.get_field('account_type').verbose_name
        self.assertEquals(field_label, 'account type')

class CourseModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)

    def test_sigle_label(self):
        course = Course.objects.get(id=1)
        field_label = course._meta.get_field('sigle').verbose_name
        self.assertEquals(field_label, 'sigle')

    def test_n_CM_Qa_label(self):
        course = Course.objects.get(id=1)
        field_label = course._meta.get_field('n_CM_Qa').verbose_name
        self.assertEquals(field_label, 'n CM Qa')

    def test_n_CM_Qb_label(self):
        course = Course.objects.get(id=1)
        field_label = course._meta.get_field('n_CM_Qb').verbose_name
        self.assertEquals(field_label, 'n CM Qb')

    def test_n_EX_Qa_label(self):
        course = Course.objects.get(id=1)
        field_label = course._meta.get_field('n_EX_Qa').verbose_name
        self.assertEquals(field_label, 'n EX Qa')

    def test_n_EX_Qb_label(self):
        course = Course.objects.get(id=1)
        field_label = course._meta.get_field('n_EX_Qb').verbose_name
        self.assertEquals(field_label, 'n EX Qb')

class StudentModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Student.objects.create(code=10)

    def test_code_label(self):
        student = Student.objects.get(id=1)
        field_label = student._meta.get_field('code').verbose_name
        self.assertEquals(field_label, 'code')

class StudentCourseModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        student = Student.objects.create(code=10)
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        StudentCourse.objects.create(student=student, course=course)

    def test_student_label(self):
        object = StudentCourse.objects.get(id=1)
        field_label = object._meta.get_field('student').verbose_name
        self.assertEquals(field_label, 'student')

    def test_course_label(self):
        object = StudentCourse.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

class SlotModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Slot.objects.create(day=1, shift=1)

    def test_day_label(self):
        slot = Slot.objects.get(id=1)
        field_label = slot._meta.get_field('day').verbose_name
        self.assertEquals(field_label, 'day')

    def test_shift_label(self):
        slot = Slot.objects.get(id=1)
        field_label = slot._meta.get_field('shift').verbose_name
        self.assertEquals(field_label, 'shift')

class ScheduleTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Schedule.objects.create(name="schedule", description="description", demi_semester=1)

    def test_name_label(self):
        schedule = Schedule.objects.get(id=1)
        field_label = schedule._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'name')

    def test_description_label(self):
        schedule = Schedule.objects.get(id=1)
        field_label = schedule._meta.get_field('description').verbose_name
        self.assertEquals(field_label, 'description')

class Slot_CM_QAModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        Slot_CM_QA.objects.create(course=course, slot=slot)

    def test_course_label(self):
        object = Slot_CM_QA.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_slot_label(self):
        object = Slot_CM_QA.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

class Slot_CM_QBModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        Slot_CM_QB.objects.create(course=course, slot=slot)

    def test_course_label(self):
        object = Slot_CM_QB.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_slot_label(self):
        object = Slot_CM_QB.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

class Slot_EX_QAModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        Slot_EX_QA.objects.create(course=course, slot=slot)

    def test_course_label(self):
        object = Slot_EX_QA.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_slot_label(self):
        object = Slot_EX_QA.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

class Slot_EX_QBModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        Slot_EX_QB.objects.create(course=course, slot=slot)

    def test_course_label(self):
        object = Slot_EX_QB.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_slot_label(self):
        object = Slot_EX_QB.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

class StudentCourseExQaModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        schedule = Schedule.objects.create(name="schedule", description="description", demi_semester=1)
        student = Student.objects.create(code=10)
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        ex_qa_slot = Slot_EX_QA.objects.create(course=course, slot=slot)
        StudentCourseExQa.objects.create(schedule=schedule, student=student, course=course, ex_qa_slot=ex_qa_slot)

    def test_schedule_label(self):
        object = StudentCourseExQa.objects.get(id=1)
        field_label = object._meta.get_field('schedule').verbose_name
        self.assertEquals(field_label, 'schedule')

    def test_student_label(self):
        object = StudentCourseExQa.objects.get(id=1)
        field_label = object._meta.get_field('student').verbose_name
        self.assertEquals(field_label, 'student')

    def test_course_label(self):
        object = StudentCourseExQa.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_ex_qa_slot_label(self):
        object = StudentCourseExQa.objects.get(id=1)
        field_label = object._meta.get_field('ex_qa_slot').verbose_name
        self.assertEquals(field_label, 'ex qa slot')


class StudentCourseExQbModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        schedule = Schedule.objects.create(name="schedule", description="description", demi_semester=1)
        student = Student.objects.create(code=10)
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        ex_qb_slot = Slot_EX_QB.objects.create(course=course, slot=slot)
        StudentCourseExQb.objects.create(schedule=schedule, student=student, course=course, ex_qb_slot=ex_qb_slot)

    def test_schedule_label(self):
        object = StudentCourseExQb.objects.get(id=1)
        field_label = object._meta.get_field('schedule').verbose_name
        self.assertEquals(field_label, 'schedule')

    def test_student_label(self):
        object = StudentCourseExQb.objects.get(id=1)
        field_label = object._meta.get_field('student').verbose_name
        self.assertEquals(field_label, 'student')

    def test_course_label(self):
        object = StudentCourseExQb.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_ex_qb_slot_label(self):
        object = StudentCourseExQb.objects.get(id=1)
        field_label = object._meta.get_field('ex_qb_slot').verbose_name
        self.assertEquals(field_label, 'ex qb slot')

class PossibilityQaModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        student = Student.objects.create(code=10)
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        ex_qa_slot = Slot_EX_QA.objects.create(course=course, slot=slot)
        PossibilityQA.objects.create(student=student, course=course, ex_qa_slot=ex_qa_slot)

    def test_student_label(self):
        object = PossibilityQA.objects.get(id=1)
        field_label = object._meta.get_field('student').verbose_name
        self.assertEquals(field_label, 'student')

    def test_course_label(self):
        object = PossibilityQA.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_ex_qa_slot_label(self):
        object = PossibilityQA.objects.get(id=1)
        field_label = object._meta.get_field('ex_qa_slot').verbose_name
        self.assertEquals(field_label, 'ex qa slot')


class PossibilityQbModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        student = Student.objects.create(code=10)
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        ex_qb_slot = Slot_EX_QB.objects.create(course=course, slot=slot)
        PossibilityQB.objects.create(student=student, course=course, ex_qb_slot=ex_qb_slot)

    def test_student_label(self):
        object = PossibilityQB.objects.get(id=1)
        field_label = object._meta.get_field('student').verbose_name
        self.assertEquals(field_label, 'student')

    def test_course_label(self):
        object = PossibilityQB.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_ex_qb_slot_label(self):
        object = PossibilityQB.objects.get(id=1)
        field_label = object._meta.get_field('ex_qb_slot').verbose_name
        self.assertEquals(field_label, 'ex qb slot')

class EventModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        schedule = Schedule.objects.create(name="schedule", description="description", demi_semester=1)
        course = Course.objects.create(sigle='LINGI2361', n_CM_Qa=1, n_CM_Qb=1, n_EX_Qa=0, n_EX_Qb=0)
        slot = Slot.objects.create(day=1, shift=1)
        Event.objects.create(schedule=schedule, course=course, description="description", slot=slot, tag=1)

    def test_schedule_label(self):
        object = Event.objects.get(id=1)
        field_label = object._meta.get_field('schedule').verbose_name
        self.assertEquals(field_label, 'schedule')

    def test_course_label(self):
        object = Event.objects.get(id=1)
        field_label = object._meta.get_field('course').verbose_name
        self.assertEquals(field_label, 'course')

    def test_description_label(self):
        object = Event.objects.get(id=1)
        field_label = object._meta.get_field('description').verbose_name
        self.assertEquals(field_label, 'description')

    def test_slot_label(self):
        object = Event.objects.get(id=1)
        field_label = object._meta.get_field('slot').verbose_name
        self.assertEquals(field_label, 'slot')






