from django.shortcuts import render
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from scheduling.models import *

@login_required
def calendar_display(request, pk):
    schedule = Schedule.objects.filter(pk=pk)
    event_objects = Event.objects.filter(schedule)

    args = {"event_objects": event_objects,}
    return render(request, 'calendar/fullcalendar.html', args)

"""
Event Views
"""
class EventListView(ListView):
    template_name = 'calendar/event/event_list.html' #indicate the template name
    model = Event
    paginate_by = 20  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class EventDetailView(DetailView):
    template_name = 'calendar/event/event.html' #indicate the template name
    model = Event

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class EventUpdateView(UpdateView):
    template_name = 'calendar/event/edit_event.html'
    model = Event
    fields = ["course", "description", "slot", "tag"]

class EventDeleteView(DeleteView):
    template_name = 'calendar/event/event_confirm_delete.html'
    model = Event
    success_url = reverse_lazy('events_list')

class EventCreateView(CreateView):
    template_name = 'calendar/event/event_form.html'
    model = Event
    fields = ["course", "description", "slot", "tag"]

