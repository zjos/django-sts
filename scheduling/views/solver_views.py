import random
import time

from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy

from scheduling.algorithms.possibility import Possibility
from scheduling.forms import ScheduleCreationForm
from scheduling.algorithms.genetic import GA
from scheduling.solver.stat import *
from scheduling.solver.scheduler import Scheduler
from scheduling.models import *
from django.shortcuts import redirect


def index(request):
    return render(request, 'solver/solver.html', {})

"""
Schedule views
"""
class ScheduleListView(ListView):
    template_name = 'solver/schedule/schedule_list.html' #indicate the template name
    model = Schedule
    paginate_by = 100  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class ScheduleDetailView(DetailView):
    template_name = 'solver/schedule/schedule.html' #indicate the template name
    model = Schedule

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['schedule'] = self.object
        context['events'] = Event.objects.filter(schedule=self.object)
        if self.object.demi_semester == 1:
            context['student_course_ex'] = StudentCourseExQa.objects.filter(schedule=self.object)
        else :
            context['student_course_ex'] = StudentCourseExQb.objects.filter(schedule=self.object)
        context["schedule_stat"] = ScheduleStat(self.object)
        courses = Course.objects.all()
        context["gen_difference_average"] = self.get_difference_average(courses)
        filtered_courses = self.course_filter(courses)
        context["red_difference_average"] = self.get_difference_average(filtered_courses)
        return context

    """
    Course filter
    """
    def course_filter(self, courses):
        result = []
        for course in courses:
            if course.n_students() > 0:
                if self.object.demi_semester == 1:
                    if course.n_EX_Qa > 0:
                        result.append(course)
                else :
                    if course.n_EX_Qb > 0:
                        result.append(course)
        return result


    """
    Get difference average for a schedule
    """
    def get_difference_average(self, courses):
        differences = 0
        for course in courses:
            if self.object.demi_semester == 1:
                labs = Slot_EX_QA.objects.filter(course=course)
                differences += self.get_course_difference_average(labs)
            else:
                labs = Slot_EX_QB.objects.filter(course=course)
                differences += self.get_course_difference_average(labs)

        if len(courses) > 0 :
            return str(float("{0:.2f}".format(differences/len(courses))))+"%"
        return str(float("{0:.2f}".format(0)))+"%"

    """
    Get difference average of a course
    """
    def get_course_difference_average(self, labs):
        differences = 0
        for lab in labs:
            differences += lab.get_difference(self.object)
        if len(labs) > 0 :
            return float("{0:.2f}".format(differences/len(labs)))
        return float("{0:.2f}".format(0))



class ScheduleUpdateView(UpdateView):
    template_name = 'solver/schedule/edit_schedule.html'
    model = Schedule
    fields = ["name", "description"]

class ScheduleDeleteView(DeleteView):
    template_name = 'solver/schedule/schedule_confirm_delete.html'
    model = Schedule
    success_url = reverse_lazy('schedules_list')

class ScheduleCreateView(CreateView):
    form_class = ScheduleCreationForm
    success_url = reverse_lazy('schedules_list')
    template_name = 'solver/schedule/new_schedule.html'

"""
Event View
"""
class EventsCreateView(CreateView):
    template_name =  'solver/schedule/scheduler-events.html'

    def get(self, request, *args, **kwargs):
        schedule = Schedule.objects.filter(pk=self.kwargs['pk']).first() # get current schedule
        courses = get_coursesForSchedule(schedule.demi_semester)

        args = {'schedule': schedule,
                'courses': courses}

        return render(request, self.template_name, args)

"""
Create events for a specifique schedule
"""
def create_events(scheduler):
    for course in scheduler.courses:
        if scheduler.schedule.demi_semester is 1: # Qa
            course_slots_CM_Qa = Slot_CM_QA.objects.filter(course=course)
            for object in course_slots_CM_Qa: # event for each slot
                Event.objects.create(schedule=scheduler.schedule,
                                     course=object.course,
                                     description=get_description(scheduler.schedule, object.course, "CM"),
                                     slot=object.slot,
                                     tag= 1) # day and shift
            course_slots_EX_Qa = Slot_EX_QA.objects.filter(course=course)
            for object in course_slots_EX_Qa: # event for each slot
                Event.objects.create(schedule=scheduler.schedule,
                                     course=object.course,
                                     description=get_description(scheduler.schedule, object.course, "EX"),
                                     slot=object.slot,
                                     tag= 2) # day and shift

        elif scheduler.schedule.demi_semester is 2: # Qb
            course_slots_CM_Qb = Slot_CM_QB.objects.filter(course=course)
            for object in course_slots_CM_Qb: # event for each slot
                Event.objects.create(schedule=scheduler.schedule,
                                     course=object.course,
                                     description=get_description(scheduler.schedule, object.course, "CM"),
                                     slot=object.slot,
                                     tag= 1) # day and shift
            course_slots_EX_Qb = Slot_EX_QB.objects.filter(course=course)
            for object in course_slots_EX_Qb: # event for each slot
                Event.objects.create(schedule=scheduler.schedule,
                                     course=object.course,
                                     description=get_description(scheduler.schedule, object.course, "EX"),
                                     slot=object.slot,
                                     tag= 2) # day and shift

"""
Get the event description TODO
"""
def get_description(schedule, course, CM_or_EX):
    result = CM_or_EX
    return result

"""
Get the courses for the schedule
"""
def get_coursesForSchedule(demi_semester):
    if demi_semester is 1: # Qa
        courses = Course.objects.filter(Q(n_CM_Qa__gte=1) | Q(n_EX_Qa__gte=1))
    elif demi_semester is 2 : # Qb
        courses = Course.objects.filter(Q(n_CM_Qb__gte=1) | Q(n_EX_Qb__gte=1))
    return courses

"""
Generate events
"""
def generate_event(request, pk):
    # get the schedule
    schedule = Schedule.objects.filter(pk=pk).first()

    # delete event for this schedule if exist
    any_event = Event.objects.filter(schedule=schedule)
    if any_event :
        any_event.delete()

    courses = get_coursesForSchedule(schedule.demi_semester)

    scheduler = Scheduler(schedule, courses)

    # We have some courses
    if scheduler.courses:
        create_events(scheduler)
        event_objects = Event.objects.filter(schedule=schedule)

    args = {'schedule': schedule,
            'event_objects': event_objects}

    return render(request, 'solver/schedule/calendar.html', args)

"""
Calendar for a specifique schedule
"""
def scheduleCalendar(request, pk):
    schedule = Schedule.objects.filter(pk=pk).first()
    event_objects = Event.objects.filter(schedule=schedule)

    args = {"event_objects": event_objects,
            "schedule": schedule}
    return render(request, 'solver/schedule/calendar.html', args)

"""
Filter courses
"""
def get_students_courses(students, schedule):
    result = []
    for student in students:
        objects = StudentCourse.objects.filter(student=student)
        filtered_courses = filter_course(objects, schedule)
        if filtered_courses :
            result += filtered_courses
    return result

"""
Filter by getting the course if it is given for a specifique schedule
"""
def filter_course(student_courses, schedule):
    result = []
    for object in  student_courses:
        if schedule.demi_semester == 1:
            if object.course.n_CM_Qa > 0 and object.course.n_EX_Qa > 0:
               result.append(object)
        else:
            if object.course.n_CM_Qb > 0 and object.course.n_EX_Qb > 0:
               result.append(object)
    return result

"""
Labs
"""
def labs(request, pk):
    schedule = Schedule.objects.filter(pk=pk).first()
    students = Student.objects.all()
    args={'schedule':schedule,
          'students': students,
          'n_students': students.count()}
    return render(request, 'solver/lab/labs.html', args)

def possibilities(request):
    students = Student.objects.all()
    possibilities_qa = PossibilityQA.objects.all()
    possibilities_qb = PossibilityQB.objects.all()
    args={'students': students,
          'possibilities_qa': possibilities_qa,
          'possibilities_qb': possibilities_qb}
    return render(request, 'solver/possibility/possibility.html', args)

"""
Delete all possibilities
"""
def deleteAllPossibilities():
    PossibilityQA.objects.all().delete()
    PossibilityQB.objects.all().delete()
"""
Get student possibilities for each course
"""
def studentPossibilities(request):
    students = Student.objects.all()#filter(code=2068)
    courses = Course.objects.all()
    if students and courses:
        # First delete all Possibilities
        deleteAllPossibilities()
        start = time.time()
        possibilities = Possibility(students)
        possibilities.save_possibilities() # save all possibilities
        end = time.time()

        stat = Stat(students, courses)
        pos = []#PossibilityQA.objects.filter(student=students.first())
        events = Event.objects.none()
        #student_courses = StudentCourse.objects.filter(student=Student.objects.filter(code=50).first())
        #for object in student_courses:
        #    e = Event.objects.filter(course=object.course)
        #    events = events | e
        args = {'stat': stat,
                'time': end-start,
                'pos': pos,
                #'event_objects': events
                }
        return render(request, 'solver/possibility/student_possibilities.html', args)
    return render(request, 'study/student/student_list', {})

"""
Get course for schedule
"""
def get_course_schedule(schedule):
    result = []
    courses = Course.objects.all()
    for course in courses:
        if schedule.demi_semester == 1 and course.n_EX_Qa > 0:
            result.append(course)
        elif schedule.demi_semester == 2 and course.n_EX_Qb > 0:
            result.append(course)
    return result

"""
Compute the lab groups
"""
def compute_lab_groups(request, pk):
    schedule = Schedule.objects.filter(pk=pk).first()
    students = Student.objects.all()
    courses = get_course_schedule(schedule)
    if students:
        if schedule.demi_semester == 1 and PossibilityQA.objects.all().exists():
            save_all_studentEX(courses, schedule) # save student exercises groups
            return redirect(schedule)
        elif schedule.demi_semester == 2 and PossibilityQB.objects.all().exists():
            save_all_studentEX(courses, schedule) # save student exercises groups
            return redirect(schedule)
        else:
            args = {'students': students}
            return render(request, 'solver/possibility/possibility.html', args)
    else:
        return render(request, 'study/student/student_list.html', {})

    args = {'schedule':schedule,
            'group_lab': True}
    return render(request, 'solver/schedule/schedule.html', args)

def getKey(item):
    return item[1]
"""
Sort students based on the possibilities they have
"""
def get_sorted_student_possibility(course, schedule):
    result = []
    student_courses = StudentCourse.objects.filter(course=course)
    for object in student_courses:
        if schedule.demi_semester == 1:
            n_possibilities = PossibilityQA.objects.filter(student=object.student, course=object.course).count()
            result.append((object.student, n_possibilities))
        else:
            n_possibilities = PossibilityQB.objects.filter(student=object.student, course=object.course).count()
            result.append((object.student, n_possibilities))
    sorted_students = sorted(result, key=getKey)
    return result

"""
Save all student labs group
"""
def save_all_studentEX(courses, schedule):

    if schedule.demi_semester == 1:
        StudentCourseExQa.objects.filter(schedule=schedule).delete() # delete all first
    else:
        StudentCourseExQb.objects.filter(schedule=schedule).delete() # delete all first

    for course in courses:
        students = get_sorted_student_possibility(course, schedule)
        for student in students: # for each student
            if schedule.demi_semester == 1:
                possibilities = PossibilityQA.objects.filter(Q(student=student[0]) & Q(course=course)) # get all possibilities for a course
                chosen_possibilities = choose_possibility(schedule, course, possibilities)
                for chosenOne in chosen_possibilities :
                    if chosenOne[0]:
                        StudentCourseExQa.objects.create(schedule=schedule,
                                                         student=student[0],
                                                         course=course,
                                                         ex_qa_slot=chosenOne[0])
            else:
                possibilities = PossibilityQB.objects.filter(Q(student=student[0]) & Q(course=course))
                chosen_possibilities = choose_possibility(schedule, course, possibilities)
                for chosenOne in chosen_possibilities :
                    if chosenOne[0]:
                        StudentCourseExQb.objects.create(schedule=schedule,
                                                         student=student[0],
                                                         course=course,
                                                         ex_qb_slot=chosenOne[0])

"""
Choose possibility
"""
def choose_possibility(schedule, course, possibilities):
    result = []
    if schedule.demi_semester == 1 :
        num_possibilities = course.n_EX_Qa
    else:
        num_possibilities = course.n_EX_Qb

    if possibilities:
        for object in possibilities:
            if schedule.demi_semester == 1 :
                n_students = StudentCourseExQa.objects.filter(Q(schedule=schedule) & Q(course=course) & Q(ex_qa_slot=object.ex_qa_slot)).count()
                result.append((object.ex_qa_slot, n_students))
            else:
                n_students = StudentCourseExQb.objects.filter(Q(schedule=schedule) & Q(course=course) & Q(ex_qb_slot=object.ex_qb_slot)).count()
                result.append((object.ex_qb_slot, n_students))
    else :
        if schedule.demi_semester == 1 :
            lab_sessions = Slot_EX_QA.objects.filter(course=course)
            for lab in lab_sessions:
                n_students = StudentCourseExQa.objects.filter(Q(schedule=schedule) & Q(course=course) & Q(ex_qa_slot=lab)).count()
                result.append((lab, n_students))
        if schedule.demi_semester == 2 :
            lab_sessions = Slot_EX_QB.objects.filter(course=course)
            for lab in lab_sessions:
                n_students = StudentCourseExQb.objects.filter(Q(schedule=schedule) & Q(course=course) & Q(ex_qb_slot=lab)).count()
                result.append((lab, n_students))
    return sorted(result, key=getKey)[:num_possibilities]

"""
Get all other labs
"""
def get_other_labs(student, schedule, course):
    result = []
    student_courses = StudentCourse.objects.filter(student=student)
    for object in student_courses:
        if schedule.demi_semester == 1:
            labs = Slot_EX_QA.objects.filter(course=course)
        if schedule.demi_semester == 2:
            labs = Slot_EX_QB.objects.filter(course=course)

"""
Slot aready taken
"""
def slot_already_taken(taken_slots, slot):
    for taken in taken_slots:
        if taken.ex_qb_slot.slot == slot:
            return True
    return False

"""
Get students stats
"""
def studentStat(request):
    students = Student.objects.all()
    courses = Course.objects.all()
    stat = Stat(students, courses)
    args = {"stat": stat,
            'event_objects': []}
    return render(request, 'solver/stat/students_stat.html', args)

"""
Get events for a given schedule
"""
def get_event(students, schedule):
    event = Event.objects.none()
    for student in students:
        courses = StudentCourse.objects.filter(student=student)
        for object in courses:
            event = event | Event.objects.filter(Q(schedule=schedule) & Q(course=object.course))
    return event

"""
Get student for GA computation
"""
def get_studentGA(course):
    student_courses = StudentCourse.objects.filter(course=course)
    resut = Student.objects.none()
    for o in student_courses:
        resut = resut | Student.objects.filter(pk=o.student.pk)
    return resut[:5]


"""
GA computation
"""
def ga_computation(request, pk):
    schedule = Schedule.objects.filter(pk=pk).first()
    course = Course.objects.filter(sigle='LGBIO1111').first() # 56 students
    students = get_studentGA(course)
    courses_list = get_students_courses(students, schedule)
    slots = Slot.objects.all()

    pop_size = 100
    n_gen = 50
    mutation = 0.5
    #result = []
    conflicts = []
    total_conflict = 0
    start = time.time()
    best_fitness_evolution = None
    average_fitness_evolution = None
    #for student in students :
    ga = GA(schedule, students, courses_list, slots, pop_size, n_gen, mutation)
    ga.multiple_generation()
    end = time.time()
    best_fitness_evolution = ga.evolutionBestFitness()
    average_fitness_evolution = ga.evolutionAverage()
    best = ga.best_solution
    parsed_matrix, student_conflicts = ga.parse_matrix(best)
    #conflicts += student_conflicts
    total_conflict += ga.get_total_conflict(student_conflicts)
    #result.append(parsed_matrix[0])

    args = {'schedule' : schedule,
            'students': students,
            'n_students': len(students),
            'ga': ga,
            'conflicts': student_conflicts,
            'n_conflicts': len(conflicts),
            'total_conflict': total_conflict,
            'best_fitness_evolution': best_fitness_evolution,
            'average_fitness_evolution': average_fitness_evolution,
            'best': ga.best_solution,
            'time': end - start}
    return render(request, 'solver/lab/ga_computation.html', args)

"""
Progress bar
"""
def progress_bar(curr, total, full_progbar):
        frac = curr/total
        filled_progbar = round(frac*full_progbar)
        print('\r', '#'*filled_progbar + '-'*(full_progbar-filled_progbar), '[{:>7.2%}]'.format(frac), end='')

def select_slot(schedule, ex_queryset, student_slots):
    best = {"slot": None,
            "count": 10000000}
    if schedule.demi_semester == 1:
        for ex_qa in ex_queryset:
            for slot in student_slots:
                if ex_qa.ex_qa_slot == slot:
                    count = StudentCourseExQa.objects.filter(course=ex_qa.course).count()
                    if count < best["count"] :
                        best["slot"] = ex_qa.ex_qa_slot
                        best["count"] = count
    elif schedule.demi_semester == 2:
        for ex_qb in ex_queryset:
            for slot in student_slots:
                if ex_qb.ex_qb_slot == slot:
                    count = StudentCourseExQb.objects.filter(course=ex_qb.course).count()
                    if count < best["count"] :
                        best["slot"] = ex_qb.ex_qb_slot
                        best["count"] = count
    return best

"""
Save student lab assignment in DB
"""
def save_studentEx(parsed_matrix, schedule):
    if schedule.demi_semester == 1:
        StudentCourseExQa.objects.filter(schedule=schedule).delete()
        for student_matrix in parsed_matrix: # for each student
            ex_qa_queryset = StudentCourseExQa.objects.none()
            for student_course in student_matrix["courses"]:
                if student_course["slots"]:
                    for ex_qa_slot in student_course["slots"]:
                        ex_qa_queryset = ex_qa_queryset | StudentCourseExQa.objects.filter(Q(course=student_course["course_model"]) | Q(ex_qa_slot=ex_qa_slot))
                    if not ex_qa_queryset :
                        slot = random.choice(student_course["slots"])
                        StudentCourseExQa.objects.create(schedule=schedule,
                                                         student=student_matrix["student"],
                                                         course=student_course["course_model"],
                                                         ex_qa_slot=slot)
                    else:
                        select = select_slot(schedule, ex_qa_queryset, student_course["slots"])
                        if select["slot"] :
                            StudentCourseExQa.objects.create(schedule=schedule,
                                                             student=student_matrix["student"],
                                                             course=student_course["course_model"],
                                                             ex_qa_slot=select["slot"])
                        else :
                            slot = random.choice(student_course["slots"])
                            StudentCourseExQa.objects.create(schedule=schedule,
                                                             student=student_matrix["student"],
                                                             course=student_course["course_model"],
                                                             ex_qa_slot=slot)
    elif schedule.demi_semester == 2:
        StudentCourseExQb.objects.filter(schedule=schedule).delete()
        for student_matrix in parsed_matrix: # for each student
            ex_qb_queryset = StudentCourseExQb.objects.none()
            for student_course in student_matrix["courses"]:
                if student_course["slots"]:
                    for ex_qb_slot in student_course["slots"]:
                        ex_qb_queryset = ex_qb_queryset | StudentCourseExQb.objects.filter(Q(course=student_course["course_model"]) | Q(ex_qb_slot=ex_qb_slot))
                    if not ex_qb_queryset :
                        slot = random.choice(student_course["slots"])
                        StudentCourseExQb.objects.create(schedule=schedule,
                                                         student=student_matrix["student"],
                                                         course=student_course["course_model"],
                                                         ex_qb_slot=slot)
                    else:
                        select = select_slot(schedule, ex_qb_queryset, student_course["slots"])
                        if select["slot"] :
                            StudentCourseExQb.objects.create(schedule=schedule,
                                                             student=student_matrix["student"],
                                                             course=student_course["course_model"],
                                                             ex_qb_slot=select["slot"])
                        else :
                            slot = random.choice(student_course["slots"])
                            StudentCourseExQb.objects.create(schedule=schedule,
                                                             student=student_matrix["student"],
                                                             course=student_course["course_model"],
                                                             ex_qb_slot=slot)