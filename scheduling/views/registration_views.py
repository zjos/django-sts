from scheduling.forms import UserProfileCreationForm
from django.urls import reverse_lazy
from django.views import generic

"""
Signup
"""
class signup(generic.CreateView):
    form_class = UserProfileCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'