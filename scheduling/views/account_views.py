from django.shortcuts import render, redirect
from django.views.generic.edit import UpdateView
from django.views.generic.detail import DetailView
from django.contrib.auth.decorators import login_required
from scheduling.forms import UserProfileChangeForm
from scheduling.models import UserProfile
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib import messages

"""
Profile user
"""
@login_required #only for logged user
def profile(request):
    user = request.user
    args={'user': user}
    return render(request, 'account/student_profile.html', args)

"""
Edit profile user
"""
@login_required #only for logged user
def edit_profile(request):
    if request.method=='POST':
        form = UserProfileChangeForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('profile')
        else:
            return redirect('edit_profile')
    else:
        form = UserProfileChangeForm(instance=request.user)
        args = {'form': form}
    return render(request, 'account/edit_profile_form.html', args)

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            #messages.success(request, 'Your password was successfully updated!')
            return redirect('profile')
        else:
            #messages.error(request, 'Please correct the error below.')
            redirect('change_password')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'account/change_password.html', {
        'form': form
    })

"""
Account profile
"""
class UserAccountDetailView(DetailView):
    template_name = 'account/user/account_profile.html' #indicate the template name
    model = UserProfile

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['account'] = UserProfile.objects.filter(pk=self.kwargs['pk']).first()
        return context

"""
Update account profile
"""
class UserAccountUpdateView(UpdateView):
    template_name = 'account/user/edit_account.html'
    model = UserProfile
    fields = ["username", "first_name", "last_name", "email"]

