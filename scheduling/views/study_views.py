import json

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy
from django.shortcuts import render, redirect
from django.db.models import Q
import yaml

from scheduling.models import *
from scheduling.enum import DAYS, SHIFTS
from scheduling.solver.utils import Parser
from scheduling.forms import StudentCourseExQaForm, StudentCourseExQbForm, UserForm


"""
Course views
"""
class CourseListView(ListView):
    template_name = 'study/course/course_list.html' #indicate the template name
    model = Course
    paginate_by = 20  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class CourseDetailView(DetailView):
    template_name = 'study/course/course.html' #indicate the template name
    model = Course

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['slots_CM_Qa'] = Slot_CM_QA.objects.filter(course=self.object)
        context['slots_CM_Qb'] = Slot_CM_QB.objects.filter(course=self.object)
        context['slots_EX_Qa'] = Slot_EX_QA.objects.filter(course=self.object)
        context['slots_EX_Qb'] = Slot_EX_QB.objects.filter(course=self.object)
        context['students'] = StudentCourse.objects.filter(course=self.object)
        context['schedules'] = Schedule.objects.all()
        return context

class CourseUpdateView(UpdateView):
    template_name = 'study/course/edit_course.html'
    model = Course
    fields = ["sigle", "n_CM_Qa", "n_CM_Qb", "n_EX_Qa", "n_EX_Qb"]

class CourseDeleteView(DeleteView):
    template_name = 'study/course/course_confirm_delete.html'
    model = Course
    success_url = reverse_lazy('courses_list')

class CourseCreateView(CreateView):
    template_name = 'study/course/course_form.html'
    model = Course
    fields = ["sigle", "n_CM_Qa", "n_CM_Qb", "n_EX_Qa", "n_EX_Qb"]

"""
Student view
"""
class StudentListView(ListView):
    template_name = 'study/student/student_list.html' #indicate the template name
    model = Student
    paginate_by = 20  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

"""
Get student's courses for a given schedule
"""
def get_studentScheduleCourse(schedule, student):
    result = []
    objects = StudentCourse.objects.filter(student=student)
    for o in objects:
        if schedule.demi_semester == 1:
            if o.course.n_CM_Qa > 0 or o.course.n_EX_Qa > 0:
                slots = StudentCourseExQa.objects.filter(Q(schedule=schedule) & Q(student=student) & Q(course=o.course))
                course_dict = {"course": o,
                               "slot": slots}
                result.append(course_dict)
        if schedule.demi_semester == 2:
            if o.course.n_CM_Qb > 0 or o.course.n_EX_Qb > 0:
                slots = StudentCourseExQb.objects.filter(Q(schedule=schedule) & Q(student=student) & Q(course=o.course))
                course_dict = {"course": o,
                               "slot": slots}
                result.append(course_dict)
    return result

"""
Get student events for a given schedule
"""
def get_studentEvent(studentCourses, schedule):
        result = Event.objects.none()
        for dict in studentCourses:
            events = Event.objects.filter(Q(course=dict["course"].course) & Q(tag=1) & Q(schedule=schedule)) # CM
            slot = get_choosen_slot(schedule, dict["course"])

            if slot :
                if schedule.demi_semester == 1:
                    events = events | Event.objects.filter(Q(schedule=schedule) & Q(course=dict["course"].course) & Q(tag=2) & Q(slot=slot.ex_qa_slot.slot)) # EX
                elif schedule.demi_semester == 2:
                    events = events | Event.objects.filter(Q(schedule=schedule) & Q(course=dict["course"].course) & Q(tag=2) & Q(slot=slot.ex_qb_slot.slot)) # EX

            result = result | events #Merge queryset
        return result

"""
Get the choosen slot
"""
def get_choosen_slot(schedule, object):
    if schedule.demi_semester == 1 :
        slots = StudentCourseExQa.objects.filter(Q(student=object.student) & Q(course=object.course)).first() # choosen slot
    elif schedule.demi_semester == 2:
        slots = StudentCourseExQb.objects.filter(Q(student=object.student) & Q(course=object.course)).first() # choosen slot
    else:
        slots = None
    return slots

"""
Student schedule
"""
def studentSchedule(request, pk_1, pk_2):
    student = Student.objects.filter(pk=pk_1).first()
    schedule = Schedule.objects.filter(pk=pk_2).first()
    studentcourses = get_studentScheduleCourse(schedule, student)
    event_objects = get_studentEvent(studentcourses, schedule)
    no_possibilities = get_course_with_no_possibility(student, studentcourses, schedule)
    args = {"student": student,
            "schedule": schedule,
            "courses_dict": studentcourses,
            "no_possibilities": no_possibilities,
            "event_objects": event_objects}
    return render(request, 'study/student/student_schedule.html', args)

"""
Get course with not possibility
"""
def get_course_with_no_possibility(student, studentcourses, schedule):
    result = []
    for dict in studentcourses:
        if schedule.demi_semester == 1:
            if PossibilityQA.objects.all().exists():
                if Slot_EX_QA.objects.filter(course=dict["course"].course).exists():
                    if not PossibilityQA.objects.filter(Q(student=student) & Q(course=dict["course"].course)).exists():
                        result.append(dict["course"].course)
        else:
            if PossibilityQB.objects.all().exists():
                if Slot_EX_QB.objects.filter(course=dict["course"].course).exists():
                    if not PossibilityQB.objects.filter(Q(student=student) & Q(course=dict["course"].course)).exists():
                        result.append(dict["course"].course)
    return result

"""
Get course labs
"""
def get_course_labs(course, schedule):
    result = []
    if schedule.demi_semester == 1:
        slots_EX_Qa = Slot_EX_QA.objects.filter(course=course)
        for lab in slots_EX_Qa:
            dict = {"session":lab,
                    "n_students":lab.n_students(schedule),
                    "student_percentage": lab.students_percentage(schedule),
                    "diff": lab.get_difference(schedule),
                    "difference": lab.difference_to_string(schedule)}
            result.append(dict)
    else:
        slots_EX_Qb = Slot_EX_QB.objects.filter(course=course)
        for lab in slots_EX_Qb:
            dict = {"session":lab,
                    "n_students":lab.n_students(schedule),
                    "student_percentage": lab.students_percentage(schedule),
                    "diff": lab.get_difference(schedule),
                    "difference": lab.difference_to_string(schedule)}
            result.append(dict)
    return result

"""
Get difference average
"""
def get_difference_average(labs):
    differences = 0
    for lab in labs:
        differences += lab["diff"]
    if len(labs) > 0 :
        return str(float("{0:.2f}".format(differences/len(labs))))+"%"
    return str(float("{0:.2f}".format(0)))+"%"

"""
Course schedule
"""
def courseSchedule(request, pk_1, pk_2):
    course = Course.objects.filter(pk=pk_1).first()
    schedule = Schedule.objects.filter(pk=pk_2).first()
    event_objects = Event.objects.filter(Q(course=course) & Q(schedule=schedule))
    labs = get_course_labs(course, schedule)
    difference_average = get_difference_average(labs)
    args = {"course": course,
            "schedule": schedule,
            "event_objects": event_objects,
            "labs": labs,
            "difference_average": difference_average}
    return render(request, 'study/course/course_schedule.html', args)

"""
Lab students list
"""
def lab_students(request, pk_1, pk_2, pk_3):
    schedule = Schedule.objects.filter(pk=pk_1).first()
    course = Course.objects.filter(pk=pk_2).first()
    if schedule.demi_semester == 1:
        slot = Slot_EX_QA.objects.filter(pk=pk_3).first()
        student_ex = StudentCourseExQa.objects.filter(Q(schedule=schedule) & Q(course=course) & Q(ex_qa_slot=slot))
    else:
        slot = Slot_EX_QB.objects.filter(pk=pk_3).first()
        student_ex = StudentCourseExQb.objects.filter(Q(schedule=schedule) & Q(course=course) & Q(ex_qb_slot=slot))
    args = {'students':student_ex,
            'schedule':schedule,
            'course':course,
            'slot':slot}
    return render(request, 'study/student/lab_students_list.html', args)

"""
Edit lab session
"""
def edit_lab_session(request, pk_1, pk_2, pk_3):
    student = Student.objects.filter(pk=pk_1).first()
    schedule = Schedule.objects.filter(pk=pk_2).first()
    course = Course.objects.filter(pk=pk_3).first()
    if request.method=='POST':
        if schedule.demi_semester == 1:
            my_instance = StudentCourseExQa.objects.filter(Q(schedule=schedule) & Q(student=student) & Q(course=course)).first()
            form = StudentCourseExQaForm(request.POST, instance=my_instance)
            if form.is_valid():
                form.save()
                studentcourses = get_studentScheduleCourse(schedule, student)
                no_possibilities = get_course_with_no_possibility(student, studentcourses, schedule)
                event_objects = get_studentEvent(studentcourses, schedule)
                args = {"student": student,
                        "schedule": schedule,
                        "courses_dict": studentcourses,
                        "no_possibilities": no_possibilities,
                        "event_objects": event_objects,
                        "form": form}
                return render(request, 'study/student/student_schedule.html', args)
        if schedule.demi_semester == 2:
            my_instance = StudentCourseExQb.objects.filter(Q(schedule=schedule) & Q(student=student) & Q(course=course)).first()
            form = StudentCourseExQbForm(request.POST, instance=my_instance)
            if form.is_valid():
                form.save()
                studentcourses = get_studentScheduleCourse(schedule, student)
                no_possibilities = get_course_with_no_possibility(student, studentcourses, schedule)
                event_objects = get_studentEvent(studentcourses, schedule)
                args = {"student": student,
                        "schedule": schedule,
                        "courses_dict": studentcourses,
                        "no_possibilities": no_possibilities,
                        "event_objects": event_objects,
                        "form": form}
                return render(request, 'study/student/student_schedule.html', args)
    else:
        if schedule.demi_semester == 1:
            my_instance = StudentCourseExQa.objects.filter(Q(schedule=schedule) & Q(student=student) & Q(course=course)).first()
            form = StudentCourseExQaForm(instance=my_instance)

            studentcourses = get_studentScheduleCourse(schedule, student)
            event_objects = get_studentEvent(studentcourses, schedule)
            args = {"student": student,
                    "schedule": schedule,
                    "course": course,
                    "courses": studentcourses,
                    "event_objects": event_objects,
                    "form": form,
                    "object": my_instance}
            return render(request, 'study/student/edit_lab_session.html', args)
        if schedule.demi_semester == 2:
            my_instance = StudentCourseExQb.objects.filter(Q(schedule=schedule) & Q(student=student) & Q(course=course)).first()
            form = StudentCourseExQbForm(instance=my_instance)

            studentcourses = get_studentScheduleCourse(schedule, student)
            event_objects = get_studentEvent(studentcourses, schedule)
            args = {"student": student,
                    "schedule": schedule,
                    "course": course,
                    "courses": studentcourses,
                    "event_objects": event_objects,
                    "form": form,
                    "object": my_instance}
            return render(request, 'study/student/edit_lab_session.html', args)

"""
Student view
"""
class StudentDetailView(DetailView):
    template_name = 'study/student/student.html' #indicate the template name
    model = Student

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['student'] = self.object
        context['schedules'] = Schedule.objects.all()
        context['courses'] = StudentCourse.objects.filter(student=self.object)
        context['event_objects'] = self.get_myEvent(context['courses'])
        return context

    def get_myEvent(self, studentCourses):
        result = Event.objects.none()
        for object in studentCourses:
            events = Event.objects.filter(Q(course=object.course) & Q(tag=1)) # CM
            slot = StudentCourseExQa.objects.filter(Q(student=object.student) & Q(course=object.course)).first() # choosen slot
            if slot :
                events = events | Event.objects.filter(Q(course=object.course) & Q(tag=2) & Q(slot=slot.ex_qa_slot.slot)) # EX
            result = result | events #Merge queryset
        return result

class StudentUpdateView(UpdateView):
    template_name = 'study/student/edit_student.html'
    model = Student
    fields = ["code"]

    def get_success_url(self):
        return reverse('student_details', kwargs={'pk': self.object.pk,})

class StudentDeleteView(DeleteView):
    template_name = 'study/student/student_confirm_delete.html'
    model = Student
    success_url = reverse_lazy('students_list')

class StudentCreateView(CreateView):
    template_name = 'study/student/student_form.html'
    model = Student
    fields = ["code"]


"""
Slot views
"""
class SlotListView(ListView):
    template_name = 'study/slot/slot_list.html' #indicate the template name
    model = Slot
    paginate_by = 100  # if pagination is desired

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class SlotDetailView(DetailView):
    template_name = 'study/slot/slot.html' #indicate the template name
    model = Slot

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['courses_lecture_qa'] = Slot_CM_QA.objects.filter(slot=self.object)
        context['courses_lab_qa'] = Slot_EX_QA.objects.filter(slot=self.object)
        context['courses_lecture_qb'] = Slot_CM_QB.objects.filter(slot=self.object)
        context['courses_lab_qb'] = Slot_EX_QB.objects.filter(slot=self.object)
        return context

class SlotUpdateView(UpdateView):
    template_name = 'study/slot/edit_slot.html'
    model = Slot
    fields = ["day", "shift"]

class SlotDeleteView(DeleteView):
    template_name = 'study/slot/slot_confirm_delete.html'
    model = Slot
    success_url = reverse_lazy('slots_list')

class SlotCreateView(CreateView):
    template_name = 'study/slot/slot_form.html'
    model = Slot
    fields = ["day", "shift"]

""""
MOVE
"""
class Move_QA_UpdateView(UpdateView):
    template_name = 'study/student/move_student.html'
    model = StudentCourseExQa
    fields = ["schedule", "student", "course", "ex_qa_slot"]

    def get_success_url(self, **kwargs):
        ex_qa_slot = self.object.ex_qa_slot
        schedule = self.object.schedule
        return reverse_lazy('lab_qa_details', kwargs = {'pk':ex_qa_slot.pk, 'pk_2':schedule.pk})

    def get_form(self, *args, **kwargs):
        form = super(Move_QA_UpdateView, self).get_form(*args, **kwargs)
        form.fields['schedule'].queryset = Schedule.objects.filter(pk=self.object.schedule.pk)
        form.fields['student'].queryset = Student.objects.filter(pk=self.object.student.pk)
        form.fields['course'].queryset = Course.objects.filter(pk=self.object.course.pk)
        form.fields['ex_qa_slot'].queryset = Slot_EX_QA.objects.filter(course=self.object.course)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["course"] = self.object.course
        context["possibility"] = PossibilityQA.objects.filter(Q(student=self.object.student) & Q(course=self.object.course))
        return context

class Move_QB_UpdateView(UpdateView):
    template_name = 'study/student/move_student.html'
    model = StudentCourseExQb
    fields = ["schedule", "student", "course", "ex_qb_slot"]

    def get_success_url(self, **kwargs):
        ex_qb_slot = self.object.ex_qb_slot
        schedule = self.object.schedule
        return reverse_lazy('lab_qb_details', kwargs = {'pk':ex_qb_slot.pk, 'pk_2':schedule.pk})

    def get_form(self, *args, **kwargs):
        form = super(Move_QB_UpdateView, self).get_form(*args, **kwargs)
        form.fields['schedule'].queryset = Schedule.objects.filter(pk=self.object.schedule.pk)
        form.fields['student'].queryset = Student.objects.filter(pk=self.object.student.pk)
        form.fields['course'].queryset = Course.objects.filter(pk=self.object.course.pk)
        form.fields['ex_qb_slot'].queryset = Slot_EX_QB.objects.filter(course=self.object.course)
        return form


"""
Lab views
"""
class Lab_QA_DetailView(DetailView):
    template_name = 'study/lab/lab_qa.html' #indicate the template name
    model = Slot_EX_QA
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        schedule = Schedule.objects.filter(pk=self.kwargs['pk_2']).first()
        context["students"] = StudentCourseExQa.objects.filter(Q(schedule=schedule) & Q(ex_qa_slot=self.object))
        context["schedule"] = schedule
        return context

class Lab_QB_DetailView(DetailView):
    template_name = 'study/lab/lab_qb.html' #indicate the template name
    model = Slot_EX_QB
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        schedule = Schedule.objects.filter(pk=self.kwargs['pk_2']).first()
        context["students"] = StudentCourseExQb.objects.filter(Q(schedule=schedule) & Q(ex_qb_slot=self.object))
        context["schedule"] = schedule
        return context

class Lab_QA_UpdateView(UpdateView):
    template_name = 'study/lab/edit_lab.html'
    model = Slot_EX_QA
    fields = ["course", "slot"]

    def get_success_url(self, **kwargs):
        return reverse_lazy('lab_qa_details', kwargs = self.kwargs)

    def get_form(self, *args, **kwargs):
        form = super(Lab_QA_UpdateView, self).get_form(*args, **kwargs)
        form.fields['course'].queryset = Course.objects.filter(pk=self.object.course.pk)
        form.fields['slot'].queryset = Slot.objects.all()
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        schedule = Schedule.objects.filter(pk=self.kwargs['pk_2']).first()
        context["schedule"] = schedule
        return context

class Lab_QB_UpdateView(UpdateView):
    template_name = 'study/lab/edit_lab.html'
    model = Slot_EX_QB
    fields = ["course", "slot"]

    def get_success_url(self, **kwargs):
        return reverse_lazy('lab_qa_details', kwargs = self.kwargs)

    def get_form(self, *args, **kwargs):
        form = super(Lab_QB_UpdateView, self).get_form(*args, **kwargs)
        form.fields['course'].queryset = Course.objects.filter(pk=self.object.course.pk)
        form.fields['slot'].queryset = Slot.objects.all()
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        schedule = Schedule.objects.filter(pk=self.kwargs['pk_2']).first()
        context["schedule"] = schedule
        return context

class Lab_QA_DeleteView(DeleteView):
    template_name = 'study/lab/lab_confirm_delete_qa.html'
    model = Slot_EX_QA

    def get_success_url(self, **kwargs):
        course = Slot_EX_QA.objects.filter(pk=self.object.pk).first().course
        return reverse_lazy('course_schedule', kwargs = {'pk_1':course.pk, 'pk_2':self.kwargs["pk_2"]})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        schedule = Schedule.objects.filter(pk=self.kwargs['pk_2']).first()
        context["schedule"] = schedule
        return context

class Lab_QB_DeleteView(DeleteView):
    template_name = 'study/lab/lab_confirm_delete_qb.html'
    model = Slot_EX_QB

    def get_success_url(self, **kwargs):
        return reverse_lazy('course_schedule', kwargs = {'pk_1':self.kwargs["pk_1"], 'pk_2':self.kwargs["pk_2"]})

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        schedule = Schedule.objects.filter(pk=self.kwargs['pk_2']).first()
        context["schedule"] = schedule
        return context

class LabCreateView(CreateView):
    template_name = 'study/slot/slot_form.html'
    model = Slot
    fields = ["day", "shift"]

"""
Generate slots
"""
def genSlots(request):
    slots = Slot.objects.all()
    if slots:
        slots.delete()
        Course.objects.all().delete()
        Student.objects.all().delete()
    slots = create_newSlots()
    args = {'slots': slots}
    return render(request, 'study/slot/gen_slot.html', args)

"""
Create new slots
"""
def create_newSlots():
    for day in DAYS:
        for shift in SHIFTS:
            if day[0] not in [6, 7] and shift[0] not in [5]:
                Slot.objects.create(day=day[0],
                                    shift=shift[0])
    return Slot.objects.all()

"""
Save courses in DB
"""
def saveCourses(courses_list):
    for course in courses_list:
        course_model = Course.objects.create(sigle=course.sigle,
                                             n_CM_Qa=course.n_CM_Qa,
                                             n_CM_Qb=course.n_CM_Qb,
                                             n_EX_Qa=course.n_EX_Qa,
                                             n_EX_Qb=course.n_EX_Qb,)
        if course.n_CM_Qa > 0:
            for code in course.slots_CM_Qa:
                slot_model = get_slot(code) # check if slot exist
                if slot_model:
                    Slot_CM_QA.objects.create(course=course_model,
                                              slot=slot_model)
        if course.n_CM_Qb > 0:
            for code in course.slots_CM_Qb:
                slot_model = get_slot(code) # check if slot exist
                if slot_model:
                    Slot_CM_QB.objects.create(course=course_model,
                                              slot=slot_model)
        if course.n_EX_Qa > 0:
            for code in course.slots_EX_Qa:
                slot_model = get_slot(code) # check if slot exist
                if slot_model:
                    Slot_EX_QA.objects.create(course=course_model,
                                              slot=slot_model)
        if course.n_EX_Qb > 0:
            for code in course.slots_EX_Qb:
                slot_model = get_slot(code) # check if slot exist
                if slot_model:
                    Slot_EX_QB.objects.create(course=course_model,
                                              slot=slot_model)

"""
Get slot
"""
def get_slot(code):
    day, shift = Parser().parse_code(code)
    slot = Slot.objects.filter(day=day,shift=shift).first()
    return slot

"""
Import Course
"""
def importCourses(request):
    slots = Slot.objects.all()
    if slots:
        if request.method == "POST":
            file = request.FILES.get('my_file', False)
            if file != False:
                courses_list = Parser().parse_courses_dict(yaml.load(file))
                courses = Course.objects.all()
                if courses:
                    courses.delete()
                    Student.objects.all().delete()
                saveCourses(courses_list)
                return redirect('courses_list')

    args = {"slots":slots}

    return render(request, 'study/course/import_courses.html', args)


"""
Save students in DB
"""
def saveStudents(students_list):
    for student in students_list:
        student_model = Student.objects.create(code=student.code)
        for course in student.courses_list:
            course_model = Course.objects.filter(sigle=course).first()
            if course_model != None:
                StudentCourse.objects.create(student=student_model, course=course_model)

"""
Import students
"""
def importStudents(request):
    courses = Course.objects.all()
    if courses :
        if request.method == "POST":
            file = request.FILES.get('my_file', False)
            if file != False:
                students_list = Parser().parse_students_dict(yaml.load(file))
                students = Student.objects.all()
                if students: # check if students already exist and delete
                    students.delete()
                saveStudents(students_list)
                return redirect('students_list')

    args = {"courses":courses}

    return render(request, 'study/student/import_students.html', args)




