from django.shortcuts import render

from django.contrib.auth.decorators import login_required

from django.conf import settings
from easy_pdf.views import PDFTemplateView
import io
from django.http import HttpResponse, Http404

class PDFView(PDFTemplateView):
    template_name = 'feature.html'

    base_url = settings.STATIC_ROOT
    download_filename = '/pdf/master_thesis_report.pdf'

    def get_context_data(self, **kwargs):
        return super(PDFView, self).get_context_data(
            pagesize='A4',
            title='Hi there Jos!',
            **kwargs
        )

def pdf_view(request):
    with open(settings.STATIC_ROOT+"/pdf/master_thesis_report.pdf", "rb") as f:
        image_data = f.read()
    return HttpResponse(image_data)

