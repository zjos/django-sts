from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from scheduling.models import *
from django import forms
from django import forms

"""
User profile creation form
"""
class UserProfileCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = UserProfile
        fields = ('username',
                  'first_name',
                  'last_name',
                  'email',
                  'account_type',
                  )

"""
User profile change form
"""
class UserProfileChangeForm(UserChangeForm):

    class Meta:
        model = UserProfile
        fields = ('username',
                  'email',
                  'first_name',
                  'last_name',
                  'account_type',
                  'password'
                  )

"""
Student course lab form for QA
"""
class StudentCourseExQaForm(forms.ModelForm):

    class Meta:
        model = StudentCourseExQa
        fields = ('schedule',
                  'student',
                  'course',
                  'ex_qa_slot',
                  )

    def __init__(self, *args, **kwargs):
        super(StudentCourseExQaForm, self).__init__(*args, **kwargs)
        self.fields['schedule'].queryset = Schedule.objects.filter(pk=kwargs['instance'].schedule.pk)
        self.fields['student'].queryset = Student.objects.filter(pk=kwargs['instance'].student.pk)
        self.fields['course'].queryset = Course.objects.filter(pk=kwargs['instance'].course.pk)
        if kwargs['instance'].schedule.demi_semester == 1 :
            self.fields['ex_qa_slot'].queryset = Slot_EX_QA.objects.filter(course=kwargs['instance'].course)
        else:
            self.fields['ex_qb_slot'].queryset = Slot_EX_QB.objects.filter(course=kwargs['instance'].course)

"""
Student course lab form for QB
"""
class StudentCourseExQbForm(forms.ModelForm):

    class Meta:
        model = StudentCourseExQb
        fields = ('schedule',
                  'student',
                  'course',
                  'ex_qb_slot',
                  )

    def __init__(self, *args, **kwargs):
        super(StudentCourseExQbForm, self).__init__(*args, **kwargs)
        self.fields['schedule'].queryset = Schedule.objects.filter(pk=kwargs['instance'].schedule.pk)
        self.fields['student'].queryset = Student.objects.filter(pk=kwargs['instance'].student.pk)
        self.fields['course'].queryset = Course.objects.filter(pk=kwargs['instance'].course.pk)
        if kwargs['instance'].schedule.demi_semester == 1 :
            self.fields['ex_qa_slot'].queryset = Slot_EX_QA.objects.filter(course=kwargs['instance'].course)
        else:
            self.fields['ex_qb_slot'].queryset = Slot_EX_QB.objects.filter(course=kwargs['instance'].course)

"""
Slot_EX form
"""
class Slot_EX_QAForm(forms.ModelForm):
    class Meta:
        model = Slot_EX_QA
        fields = ('course',
                  'slot')
    def __init__(self, *args, **kwargs):
        super(Slot_EX_QAForm, self).__init__(*args, **kwargs)
        self.fields['course'].queryset = Course.objects.filter(pk=kwargs['instance'].course.pk)
        self.fields['slot'].queryset = Slot.objects.filter(pk=kwargs['instance'].slot.pk)

class Slot_EX_QBForm(forms.ModelForm):
    class Meta:
        model = Slot_EX_QA
        fields = ('course',
                  'slot')
    def __init__(self, *args, **kwargs):
        super(Slot_EX_QBForm, self).__init__(*args, **kwargs)
        self.fields['course'].queryset = Course.objects.filter(pk=kwargs['instance'].course.pk)
        self.fields['slot'].queryset = Slot.objects.filter(pk=kwargs['instance'].slot.pk)


"""
Event creation form
"""
class EventCreationForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = ('schedule',
                  'course',
                  'description',
                  'slot',
                  )
"""
Schedule creation form
"""
class ScheduleCreationForm(forms.ModelForm):

    class Meta:
        model = Schedule
        fields = ('name',
                  'description',
                  'demi_semester'
                  )

"""
Uploade file form
"""
class UploadFileForm(forms.Form):
    title = forms.CharField(max_length=50)
    file = forms.FileField()

class UserForm(forms.ModelForm):
    class Meta:
        model = Calcu
        fields = [
            "n",
        ]

