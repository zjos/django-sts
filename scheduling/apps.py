# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function

from django.apps import AppConfig

from ortools.linear_solver import pywraplp, pywraplp
from ortools.constraint_solver import pywrapcp

class SchedulingConfig(AppConfig):
    name = 'scheduling'
