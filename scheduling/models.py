# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import print_function
from scheduling.enum import *
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Q
from django.urls import reverse
from scheduling.solver.utils import Parser

# Create your models here.


"""
UserProfile model
"""
class UserProfile(AbstractUser):
    # First/last name is not a global-friendly pattern
    account_type = models.IntegerField(choices=ACCOUNT_TYPE, default=1)

    def __str__(self):
        return self.email

    def get_account(self):
        return Parser().parse_account(self.account_type)

    def get_absolute_url(self):
        return reverse('profile', kwargs={'pk': self.pk})

"""
Course model
"""
class Course(models.Model):
    sigle = models.CharField(max_length=50, blank=False)
    n_CM_Qa = models.IntegerField(default=0, blank=False)
    n_CM_Qb = models.IntegerField(default=0, blank=False)
    n_EX_Qa = models.IntegerField(default=0, blank=False)
    n_EX_Qb = models.IntegerField(default=0, blank=False)

    def __str__(self):
        result = self.sigle
        return result

    def get_absolute_url(self):
        return reverse('course_details', kwargs={'pk': self.pk})

    def n_students(self):
        return StudentCourse.objects.filter(course=self).count()

    def n_student_group_qa(self):
        n_labs = Slot_EX_QA.objects.filter(course=self).count()
        if n_labs > 0:
            return self.n_students()/n_labs
        return 0

    def n_student_group_qb(self):
        n_labs = Slot_EX_QB.objects.filter(course=self).count()
        if n_labs > 0:
            return self.n_students()/n_labs
        return 0

    def get_average_difference_qb(self, schedule):
        n_per_group = self.n_student_group_qa()
        if n_per_group > 0:
            labs = Slot_EX_QB.objects.filter(Q(schedule=schedule) & Q(course=self))
            differences = 0
            for lab in labs:
                differences += lab.get_difference()
            return str(float("{0:.2f}".format((differences/labs.count()))))+"%"
        return 0

"""
Student model
"""
class Student(models.Model):
    code = models.IntegerField(blank=False)

    def __str__(self):
        result = str(self.code)
        return result

"""
Student course relation
"""
class StudentCourse(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)


"""
Slot model
"""
class Slot(models.Model):
    day = models.IntegerField(choices=DAYS, help_text="Select a day", default=1, blank=False)
    shift = models.IntegerField(choices=SHIFTS, help_text="Select a shift", default=1, blank=False)

    def __str__(self):
        result = Parser().parse_day(self.day)+", "+Parser().parse_shift(self.shift)
        return result

    def get_code(self):
        day_code = Parser().get_day_code(self.day)
        shift_code = Parser().get_shift_code(self.shift)
        return day_code+shift_code

    def get_absolute_url(self):
        return reverse('slot_details', kwargs={'pk': self.pk})

    def get_readable_day(self):
        return Parser().parse_day(self.day)

    def get_readable_shift(self):
        return Parser().parse_shift(self.shift)

"""
Schedule model
"""
class Schedule(models.Model):
    name = models.CharField(max_length=50, blank=False)
    description = models.TextField(blank=True)
    demi_semester = models.IntegerField(choices=DEMISEMESTER, default=1, blank=False)

    def __str__(self):
        return self.name

    def get_readable_demiSemester(self):
        return Parser().parse_demiSemester(self.demi_semester)

    def get_absolute_url(self):
        return reverse('schedule_details', kwargs={'pk': self.pk})

"""
Slot CM and EX model
"""

class Slot_CM_QA(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    slot = models.ForeignKey(Slot, on_delete=models.CASCADE)

class Slot_CM_QB(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    slot = models.ForeignKey(Slot, on_delete=models.CASCADE)

class Slot_EX_QA(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    slot = models.ForeignKey(Slot, on_delete=models.CASCADE)

    def __str__(self):
        result = self.slot.get_readable_day()+", "+self.slot.get_readable_shift()
        return result

    def n_students(self, schedule):
        return StudentCourseExQa.objects.filter(Q(schedule=schedule) & Q(course=self.course) & Q(ex_qa_slot=self)).count()

    def students_percentage(self, schedule):
        total_course_student = StudentCourse.objects.filter(Q(course=self.course)).count()
        if total_course_student != 0:
            return str(float("{0:.2f}".format((self.n_students(schedule)/total_course_student)*100)))+"%"
        else:
            return str(float("{0:.2f}".format((0)*100)))+"%"

    def get_difference(self, schedule):
        n_per_group = self.course.n_student_group_qa()
        if n_per_group > 0:
            return (abs(self.n_students(schedule)-n_per_group)/n_per_group)*100
        return 0

    def difference_to_string(self, schedule):
        return str(float("{0:.2f}".format(self.get_difference(schedule))))+"%"

    def get_absolute_url(self):
        return reverse('course_details', kwargs={'pk': self.course.pk})


class Slot_EX_QB(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    slot = models.ForeignKey(Slot, on_delete=models.CASCADE)

    def __str__(self):
        result = self.course.sigle +", "+self.slot.get_readable_day()+", "+self.slot.get_readable_shift()
        return result

    def n_students(self, schedule):
        return StudentCourseExQb.objects.filter(Q(schedule=schedule) & Q(course=self.course) & Q(ex_qb_slot=self)).count()

    def students_percentage(self, schedule):
        total_course_student = StudentCourse.objects.filter(Q(course=self.course)).count()
        if total_course_student != 0:
            return str(float("{0:.2f}".format((self.n_students(schedule)/total_course_student)*100)))+"%"
        else:
            return str(float("{0:.2f}".format((0)*100)))+"%"

    def get_difference(self, schedule):
        n_per_group = self.course.n_student_group_qb()
        if n_per_group > 0:
            return (abs(self.n_students(schedule)-n_per_group)/n_per_group)*100
        return 0

    def difference_to_string(self, schedule):
        return str(float("{0:.2f}".format(self.get_difference(schedule))))+"%"

    def get_absolute_url(self):
        return reverse('course_details', kwargs={'pk': self.course.pk})

"""
QA Student course lab slot
"""
class StudentCourseExQa(models.Model):
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    ex_qa_slot = models.ForeignKey(Slot_EX_QA, on_delete=models.CASCADE)

    def get_possibilities(self):
        return PossibilityQA.objects.filter(Q(course=self.course) & Q(student=self.student))


"""
QB Student course lab slot
"""
class StudentCourseExQb(models.Model):
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    ex_qb_slot = models.ForeignKey(Slot_EX_QB, on_delete=models.CASCADE)

    def get_possibilities(self):
        return PossibilityQB.objects.filter(Q(course=self.course) & Q(student=self.student))

    def get_absolute_url(self):
        return reverse('course_schedule', kwargs={'pk_1': self.schedule.pk, 'pk_2':self.course.pk})

"""
QA Possibility model
"""
class PossibilityQA(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    ex_qa_slot = models.ForeignKey(Slot_EX_QA, on_delete=models.CASCADE)

    def __str__(self):
        result = self.course.sigle +", "+self.ex_qa_slot.slot.get_readable_day()+", "+self.ex_qa_slot.slot.get_readable_shift()
        return result

    def readable_slot(self):
        return self.ex_qa_slot.slot

"""
QB Possibility model
"""
class PossibilityQB(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    ex_qb_slot = models.ForeignKey(Slot_EX_QB, on_delete=models.CASCADE)

    def __str__(self):
        result = self.course.sigle +", "+self.ex_qb_slot.slot.get_readable_day()+", "+self.ex_qb_slot.slot.get_readable_shift()
        return result

    def readable_slot(self):
        return self.ex_qb_slot.slot

"""
Event model
"""
class Event(models.Model):
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    slot = models.ForeignKey(Slot, on_delete=models.CASCADE)
    tag = models.IntegerField(choices=TAG, blank=False)

    def __str__(self):
        result = ""
        result += str(self.course)+", "+str(self.slot)
        return result

    def get_color(self):
        if self.tag is 2:
            return "orange"

    def get_lec_start(self):
        return self.parse_shift(self.slot.shift, True)

    def get_lec_end(self):
        return self.parse_shift(self.slot.shift, False)


    def get_readable_tag(self):
        return Parser().parse_tag(self.tag)

    def get_readable_lec_day(self):
        return Parser().parse_day(self.lec_day)

    def get_readable_lab_day(self):
        return Parser().parse_day(self.lab_day)

    def get_readable_lec_shift(self):
        return Parser().parse_shift(self.lec_shift)

    def get_readable_lab_shift(self):
        return Parser().parse_shift(self.lab_shift)

    def get_absolute_url(self):
        return reverse('event_details', kwargs={'pk': self.pk})

    def get_lab_start(self):
        return self.parse_shift(self.lab_shift, True)

    def get_lab_end(self):
        return self.parse_shift(self.lab_shift, False)

    def parse_shift(self, shift, bool):
        if shift is 1:
            if bool:
                return "08:30"
            else:
                return "10:30"
        elif shift is 2:
            if bool:
                return "10:45"
            else:
                return "12:45"
        elif shift is 3:
            if bool:
                return "14:00"
            else:
                return "16:00"
        elif shift is 4:
            if bool:
                return "16:15"
            else:
                return "18:15"
        else:
            if bool:
                return "18:30"
            else:
                return "20:30"

class Calcu(models.Model):
    n = models.CharField(max_length=10)


