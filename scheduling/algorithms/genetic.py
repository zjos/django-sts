import random
import operator
from scheduling.models import Slot_CM_QA, Slot_EX_QA, Slot_CM_QB, Slot_EX_QB, StudentCourse

class GA():
    """
    Genetic algorithm :
    A schedule solution (individual) will be represented by a boolean matrix for each student,
    row representing time slot and column course slot.
        - students : list of students
        - courses : list of courses
        - size_population : size of a population
        - numb_best_individual : number of best individual to be selected
        - numb_lucky_few_individual : number of individual to be selected randomly
        - numb_child : number of child for each breeders couple
        - numb_generation : number of generation to generate
        - numb_mutation : number of mutant for a population
        - historic : list of all generation
    """
    def __init__(self, schedule, students, courses, slots, size_population, numb_generation, chance_of_mutation):
        self.schedule = schedule
        self.students = students
        self.courses = courses
        self.student_courses_info = self.get_students_info()
        self.slots = slots
        self.size_population = size_population
        self.set_param() # num_best_individual, num_lucky_few_individual, numb_of_child
        self.numb_of_generation = numb_generation
        self.numb_mutation = int(size_population * chance_of_mutation)
        self.historic = [] # list of all generation
        self.best_solution = self.init_best_solution()

    def __str__(self):
        result = "\n"
        result += "\t === Genetic Algorithm === \n"
        result += "\n"
        result += "- Number of student(s) : "+str(len(self.students))+"\n"
        result += "- Number of course(s) : "+str(len(self.courses))+"\n"
        result += "- Population size : "+str(self.size_population)+"\n"
        result += "- Number of generation(s) : "+str(self.numb_of_generation)+"\n"
        result += "- Generation(s) generated : "+str(len(self.historic))+"\n"
        result += "\n"

        return result

    """
    Init best solution
    """
    def init_best_solution(self):
        return {"student_matrix": None,
                "perf":(0, (100000000000, 100000000)),
                "conflicts": None}

    """
    Set param
    """
    def set_param(self):
        n_best_individual = int(self.size_population * 0.1)
        if n_best_individual == 0:
            self.numb_best_individual = 1
            self.numb_lucky_few_individual = 0
            self.numb_of_child = self.size_population
        else:
            self.numb_best_individual = n_best_individual
            self.numb_lucky_few_individual = int(self.size_population * 0.1)
            self.numb_of_child = int(self.size_population / (self.numb_best_individual+self.numb_lucky_few_individual))

    """
    Initiate balance
    """
    def init_balance(self):
        result = {}
        for object in self.courses:
            if self.schedule.demi_semester == 1:
                slots = Slot_EX_QA.objects.filter(course=object.course)
                result[object.course.sigle] = {"n_students": StudentCourse.objects.filter(course=object.course).count(),
                                               "balance": []}
                for s in slots:
                    result[object.course.sigle]["balance"].append(0)
            else:
                slots = Slot_EX_QB.objects.filter(course=object.course)
                result[object.course.sigle] = {"n_students": StudentCourse.objects.filter(course=object.course).count(),
                                               "balance": []}
                for s in slots:
                    result[object.course.sigle]["balance"].append(0)
        return result

    """
    Get historic length
    """
    def get_historic_length(self):
        return len(self.historic)

    """
    Get the best individual from historic
    """
    def get_best_individual(self):
        current_best = 100000000000
        individual = {}
        for gen in self.historic:
            best_from = self.get_bestFromGen(gen) # get best individual
            if best_from["perf"][1][0] < current_best :
                current_best = best_from["perf"][1][0]
                individual["gen"] = self.historic.index(gen)
                individual["student_matrix"] = best_from["student_matrix"]
                individual["perf"] = best_from["perf"]

        return individual

    """
    Historic to string
    """
    def historic_toString(self):
        result = []
        for gen in self.historic:
            best = self.get_bestFromGen(gen) # get best individual
            result.append(self.individual_toString(best))

        return result

    """
    Individual to string
    """
    def individual_toString(self, individual):
        result = []
        result.append("Prob to be taken = "+str(individual["perf"][0])+", score ="+str(individual["perf"][1][0])+", conflicts ="+str(individual["perf"][1][1]))
        for i in range(len(individual["student_matrix"])):
            result.append("student ="+str(self.student_courses_info[i]["student"]))
            for row in individual["student_matrix"][i]:
                result.append(row)
        return result

    """
    Get the best individual from a given population
    """
    def get_bestFromGen(self, population):
        sortedPop = self.compute_perfPopulation(population) # evaluate a population
        index = sortedPop[0][0]
        evaluation = sortedPop[0][1]
        conflicts = self.individual_conflict(population[index])
        return {"student_matrix": population[index],
                "perf": evaluation,
                "conflicts": conflicts}

    def individual_conflict(self, individual):
        individual_conflict = []
        for i in range(len(individual)): # for each student
            student_info = self.student_courses_info[i]
            n_conflict = 0
            for j in range(len(student_info["student_course"])): # each courses
                student_course_info = student_info["courses"][j] # get course info
                conflicts, slots = self.conflict_evaluation(individual[i], student_course_info)
                if conflicts > 0 :
                    n_conflict += conflicts
            if n_conflict > 0:
                dict = {'student': student_info["student"],
                        'n_conflict': n_conflict}
                individual_conflict.append(dict)
        return individual_conflict

    """
    Course conflict evalution
    """
    def conflict_evaluation(self, matrix, course_info):
        val = 0
        conflict = 0
        slots = []
        if course_info["ex"]["range"] != None :
            for row in range(len(matrix)): # time slots
                if matrix[row].count(1) > 0 :
                    for col in range(course_info["ex"]["range"][0], course_info["ex"]["range"][1]): # course slots
                        if course_info["ex"]["range"][0] <= col < course_info["ex"]["range"][1]:
                            score = self.conflict_ex_time_slot_checkup(matrix, row, col, course_info["ex"]["slots"], course_info["ex"]["range"])

                            val += score
                            if (col == len(matrix[0])-1 and matrix[row].count(1) > 1):
                                conflict += matrix[row].count(1)
                                slots.append(row)
            if val == 0:
                conflict += 1
                slots.append("E")
        return conflict, slots

    def conflict_ex_time_slot_checkup(self, matrix, current_slot, current_course_slot, time_slot_queryset, course_range):
        list_index = list(range(course_range[0], course_range[1]))
        index = list_index.index(current_course_slot)
        object = time_slot_queryset[index]
        if current_slot == self.parse_time_slots(object.slot):
            if matrix[current_slot][current_course_slot] == 1:
                return 1
        return 0

    """
    Matrix to string
    """
    def matrix_toString(self, matrix):
        result = ""

        for row in range(len(matrix)):
            for col in range(len(matrix[0])):
                result += str(matrix[row][col]) + " "
            result += "\n"
        return result


    """
    Generate multiple Generation
    """
    def multiple_generation(self):
        self.historic.append(self.generateFirstPopulation()) # generation 0
        for i in range (self.numb_of_generation):
            best = self.get_bestFromGen(self.historic[i])
            """
            if best["perf"][1][1] == 0: # no conflict
                self.best_solution["student_matrix"] = best["student_matrix"]
                self.best_solution["perf"] = best["perf"]
                self.best_solution["conflicts"] = best["conflicts"]
                break
            """""
            if best["perf"][1][1] <= self.best_solution["perf"][1][1]:
                if best["perf"][1][0] < self.best_solution["perf"][1][0]:
                    self.best_solution["student_matrix"] = best["student_matrix"]
                    self.best_solution["perf"] = best["perf"]
                    self.best_solution["conflicts"] = best["conflicts"]
            self.historic.append(self.next_generation(self.historic[i]))

        best = self.get_bestFromGen(self.historic[len(self.historic)-1])
        if best["perf"][1][1] <= self.best_solution["perf"][1][1]:
            if best["perf"][1][0] < self.best_solution["perf"][1][0]:
                self.best_solution["student_matrix"] = best["student_matrix"]
                self.best_solution["perf"] = best["perf"]
                self.best_solution["conflicts"] = best["conflicts"]


    """
    Generate first population
    """
    def generateFirstPopulation(self):
        population = []
        i = 0
        while i < self.size_population:
            population.append(self.generate_individual())
            i+=1
        return population

    """
    Get all courses student infos
    """
    def get_students_info(self):
        result = []
        for student in self.students:
            student_course_instance = StudentCourse.objects.filter(student=student)
            total_slots, courses = self.get_total_course_slots(student_course_instance)
            dictionary = {'student': student,
                          'courses': courses,
                          'total_slots': total_slots,
                          'student_course': student_course_instance}
            result.append(dictionary)
        return result

    """
    get all session slot and define the differents ranges
    """
    def get_total_course_slots(self, student_courses_instance):
        total = 0
        courses = []
        for object in student_courses_instance:
            if self.schedule.demi_semester == 1:
                students = StudentCourse.objects.filter(course=object.course)
                slots_cm_qa = Slot_CM_QA.objects.filter(course=object.course)
                slots_ex_qa = Slot_EX_QA.objects.filter(course=object.course)
                n_cm_slots = slots_cm_qa.count()
                n_ex_slots = slots_ex_qa.count()
                if n_cm_slots == 0:
                    cm_range = None # No magistral courses
                else:
                    cm_range = (total, total + n_cm_slots) # magistral courses range
                total += n_cm_slots
                if n_ex_slots == 0:
                    ex_range = None # No exercise
                else:
                    ex_range = (total, total + n_ex_slots) # exercise range
                total += n_ex_slots
                courses.append({"sigle": object.course.sigle,
                                "n_students": students.count(),
                                "cm": {"n": object.course.n_CM_Qa, "range": cm_range, "slots": slots_cm_qa},
                                "ex": {"n": object.course.n_EX_Qa, "range": ex_range, "slots": slots_ex_qa}
                               })
            elif self.schedule.demi_semester == 2:
                students = StudentCourse.objects.filter(course=object.course)
                slots_cm_qb = Slot_CM_QB.objects.filter(course=object.course)
                slots_ex_qb = Slot_EX_QB.objects.filter(course=object.course)
                n_cm_slots = slots_cm_qb.count()
                n_ex_slots = slots_ex_qb.count()
                if n_cm_slots == 0:
                    cm_range = None # No magistral courses
                else:
                    cm_range = (total, total + n_cm_slots) # magistral courses range
                total += n_cm_slots
                if n_ex_slots == 0:
                    ex_range = None # No exercise
                else:
                    ex_range = (total, total + n_ex_slots) # exercise range
                total += n_ex_slots
                courses.append({"sigle": object.course.sigle,
                                "n_students": students.count(),
                                "cm": {"n": object.course.n_CM_Qb, "range": cm_range, "slots": slots_cm_qb},
                                "ex": {"n": object.course.n_EX_Qb, "range": ex_range, "slots": slots_ex_qb}
                               })

        return total, courses

    """
    Generate individual : a list with boolean matrix of each student
    """
    def generate_individual(self):
        individual = []
        for student_info in self.student_courses_info: # each student
            matrix = self.generate_matrix(student_info)
            individual.append(matrix)
        return individual

    """
    Generate boolean matrix for a student
    """
    def generate_matrix(self, student_info):
        time_slot = []
        i = 0
        for row in range(len(self.slots)): # time slots
            course_slot = []
            for student_course_info in student_info["courses"]: # each courses of a student
                if self.time_slots_checkup(row, student_course_info):
                    if student_course_info["cm"]["range"] != None and student_course_info["ex"]["range"] != None:
                        for col in range(student_course_info["cm"]["range"][0], student_course_info["ex"]["range"][1]): # course slots
                            value = self.set_course_matrix_value(row, col, student_course_info)
                            course_slot.append(value)
                    elif student_course_info["cm"]["range"] != None and student_course_info["ex"]["range"] == None:
                        for col in range(student_course_info["cm"]["range"][0], student_course_info["cm"]["range"][1]): # course slots
                            value = self.set_course_matrix_value(row, col, student_course_info)
                            course_slot.append(value)
                    elif student_course_info["cm"]["range"] == None and student_course_info["ex"]["range"] != None:
                        for col in range(student_course_info["ex"]["range"][0], student_course_info["ex"]["range"][1]): # course slots
                            value = self.set_course_matrix_value(row, col, student_course_info)
                            course_slot.append(value)
                else:
                    if student_course_info["cm"]["range"] != None and student_course_info["ex"]["range"] != None:
                        for col in range(student_course_info["cm"]["range"][0], student_course_info["ex"]["range"][1]): # course slots
                            course_slot.append(0)
                    elif student_course_info["cm"]["range"] != None and student_course_info["ex"]["range"] == None:
                        for col in range(student_course_info["cm"]["range"][0], student_course_info["cm"]["range"][1]): # course slots
                            course_slot.append(0)
                    elif student_course_info["cm"]["range"] == None and student_course_info["ex"]["range"] != None:
                        for col in range(student_course_info["ex"]["range"][0], student_course_info["ex"]["range"][1]): # course slots
                            course_slot.append(0)
            time_slot.append(course_slot)
        return time_slot


    """
    Time slot checkup returns true if time slot is used by a given course otherwise false
    """
    def time_slots_checkup(self, current_slot, student_course_info):
        cm_slots = student_course_info["cm"]["slots"]
        ex_slots = student_course_info["ex"]["slots"] # get slots

        if self.slots_checkup(current_slot, cm_slots):
            return True
        else :
            return self.slots_checkup(current_slot, ex_slots)

    """
    Slot checkup
    """
    def slots_checkup(self, current_slot, slots):
        for object in slots:
            if current_slot == self.parse_time_slots(object.slot):
                return True
        return False

    """
    Set a boolean value (0, 1) for M(r,c)
    """
    def set_course_matrix_value(self, row, col, student_course_info):
        if student_course_info["cm"]["range"] != None :
            if student_course_info["cm"]["range"][0] <= col < student_course_info["cm"]["range"][1]:
                return self.cm_time_slot_assign(row, col, student_course_info) # CM slot
        if student_course_info["ex"]["range"] != None :
            if student_course_info["ex"]["range"][0] <= col < student_course_info["ex"]["range"][1]:
                return self.ex_time_slot_assign(row, col, student_course_info) # EX slot
        return None

    """
    CM time slot : No choice
    """
    def cm_time_slot_assign(self, current_slot, current_course_slot, student_course_info):
        course_range = student_course_info["cm"]["range"] # get range
        list_index = list(range(course_range[0], course_range[1]))
        time_slot_queryset = student_course_info["cm"]["slots"]
        index = list_index.index(current_course_slot) # get queryset index

        object = time_slot_queryset[index]

        if current_slot == self.parse_time_slots(object.slot):
            return 1
        return 0

    """
    EX time slot : choice available
    """
    def ex_time_slot_assign(self, current_slot, current_course_slot, student_course_info):
        course_range = student_course_info["ex"]["range"] # get range
        list_index = list(range(course_range[0], course_range[1]))
        time_slot_queryset = student_course_info["ex"]["slots"]
        index = list_index.index(current_course_slot) # get queryset index

        object = time_slot_queryset[index]

        if current_slot == self.parse_time_slots(object.slot):
            return random.randint(0, 1) #random value
        return 0

    """
    Parse time slots
    """
    def parse_time_slots(self, time_slot):
        if time_slot.day is 1:
            if time_slot.shift is 1:
                return 0
            if time_slot.shift is 2:
                return 1
            if time_slot.shift is 3:
                return 2
            if time_slot.shift is 4:
                return 3
            else:
                return None
        elif time_slot.day is 2:
            if time_slot.shift is 1:
                return 4
            if time_slot.shift is 2:
                return 5
            if time_slot.shift is 3:
                return 6
            if time_slot.shift is 4:
                return 7
            else:
                return None
        elif time_slot.day is 3:
            if time_slot.shift is 1:
                return 8
            if time_slot.shift is 2:
                return 9
            if time_slot.shift is 3:
                return 10
            if time_slot.shift is 4:
                return 11
            else:
                return None
        elif time_slot.day is 4:
            if time_slot.shift is 1:
                return 12
            if time_slot.shift is 2:
                return 13
            if time_slot.shift is 3:
                return 14
            if time_slot.shift is 4:
                return 15
            else:
                return None
        elif time_slot.day is 5:
            if time_slot.shift is 1:
                return 16
            if time_slot.shift is 2:
                return 17
            if time_slot.shift is 3:
                return 18
            if time_slot.shift is 4:
                return 19
            else:
                return None
        else :
            return None


    """
    Get a specifique course range
    """
    def get_student_course(self, student_courses, current_course):
        for course in student_courses:
            if course["sigle"] == current_course.sigle:
                return course
        return None

    """
    Generate the next generation
    """
    def next_generation (self, current_generation):
         perf_sorted = self.compute_perfPopulation(current_generation) # evaluation of the current generation
         nextBreeders = self.select_breeders(perf_sorted) # choose parent from the next generation
         nextPopulation = self.create_children(nextBreeders, current_generation)
         nextGeneration = self.mutate_population(nextPopulation)
         return nextGeneration

    """
    get the population from index and each individual will be associated to his fitness
    """
    def get_pop(self, generation, index_pop):
        result = []
        for index, perf in index_pop:
            result.append((generation[index], perf))

        return result

    """
    Mutate a population by choosing some individual
    """
    def mutate_population(self, population):
        next_mutants = random.sample(range(0, self.size_population), self.numb_mutation)
        for i in range(len(population)):
            if i in next_mutants:
                population[i] = self.mutate_individual(population[i])
        return population

    """
    Mutate an individual
    """
    def mutate_individual(self, individual):
        result = []
        for i in range(len(individual)): # student matrix
            matrix = individual[i] # boolean matrix of the course
            row = []
            for r in range(len(matrix)):
                col = []
                for c in range(len(matrix[0])):
                    student_info = self.student_courses_info[i]
                    for object in student_info["student_course"]: # each courses
                        student_course_info = self.get_student_course(student_info["courses"], object.course) # get the ranges course
                        col = self.mutant_matrix(student_course_info, matrix, r, c, col)
                row.append(col)
            result.append(row)
        return result

    """
    Mutate a matrix by change some column
    """
    def mutant_matrix(self, student_course_info, matrix, r, c, col):

        if self.time_slots_checkup(r, student_course_info):
            if student_course_info["ex"]["range"] != None :
                if student_course_info["ex"]["range"][0] <= c < student_course_info["ex"]["range"][1] :
                    if random.randint(0, 1) == 0:
                        col.append(abs(matrix[r][c]-1))
                    else:
                        col.append(matrix[r][c])
            if student_course_info["cm"]["range"] != None :
                if student_course_info["cm"]["range"][0] <= c < student_course_info["cm"]["range"][1]:
                    col.append(matrix[r][c])
        else:
            if student_course_info["ex"]["range"] != None :
                if student_course_info["ex"]["range"][0] <= c < student_course_info["ex"]["range"][1] :
                    col.append(matrix[r][c])
            if student_course_info["cm"]["range"] != None :
                if student_course_info["cm"]["range"][0] <= c < student_course_info["cm"]["range"][1]:
                    col.append(matrix[r][c])

        return col


    """
    Create children for the next generation from breeders 20
    """
    def create_children(self, breeders, generation):
        nextPopulation = []
        for i in range(len(breeders)):
            for j in range(self.numb_of_child):

                breederA = generation[breeders[i][0]]
                breederB = generation[breeders[len(breeders) -1 -i][0]]

                nextPopulation.append(self.create_child(breederA, breederB))

        return nextPopulation

    """
    Create a child by crossover operation
    """
    def create_child(self, individualA, individualB):
        individual_child = []
        for i in range(len(individualA)):
            student_info = self.student_courses_info[i]
            matrixA = individualA[i] # get the student matrix A
            matrixB = individualB[i] # get the student matrix B
            child_matrix = self.crossover(student_info, matrixA, matrixB) # crossover operation
            individual_child.append(child_matrix)
        return individual_child

    """
    Crossover :
    """
    def crossover(self, student_info, matrixA, matrixB):
        matrix_list = [0, 1]
        result = []
        for r in range(len(matrixA)):
            col = []
            if matrixA[r].count(1) == 0 and matrixB[r].count(1) == 0:
                for i in range(len(matrixA[0])):
                    col.append(0)
            else :
                for c in range(len(matrixA[0])):
                    for object in student_info["student_course"]: # each courses
                        choice = random.choice(matrix_list) # choose randomly a matrix
                        student_course_info = self.get_student_course(student_info["courses"], object.course) # get the ranges course
                        if self.time_slots_checkup(r, student_course_info):
                            if student_course_info["cm"]["range"][0] <= c < student_course_info["cm"]["range"][1]:
                                col.append(matrixA[r][c])
                            if student_course_info["ex"]["range"] != None :
                                if student_course_info["ex"]["range"][0] <= c < student_course_info["ex"]["range"][1] :
                                    value = self.ex_time_slot_crossover(matrixA, matrixB, choice, r, c, student_course_info)
                                    col.append(value)
                        else:
                            if student_course_info["cm"]["range"][0] <= c < student_course_info["cm"]["range"][1]:
                                col.append(matrixA[r][c])
                            if student_course_info["ex"]["range"] != None :
                                if student_course_info["ex"]["range"][0] <= c < student_course_info["ex"]["range"][1] :
                                    col.append(matrixA[r][c])
            result.append(col)
        return result

    """
    Ex time slot crossover
    """
    def ex_time_slot_crossover(self, matrixA, matrixB, choice, current_slot, current_course_slot, student_course_info):
        course_range = student_course_info["ex"]["range"]
        list_index = list(range(course_range[0], course_range[1]))
        index = list_index.index(current_course_slot)
        time_slot_queryset = student_course_info["ex"]["slots"]
        object = time_slot_queryset[index]

        if current_slot == self.parse_time_slots(object.slot):
            if choice == 0:
                return matrixA[current_slot][current_course_slot]
            else:
                return matrixB[current_slot][current_course_slot]
        else :
            return 0

    """
    Select the next breeders from the current sorted population
        - % of best individual
        - % of random individual
    """
    def select_breeders(self, populationSorted):
        next_breeders = []
        if self.numb_best_individual+self.numb_lucky_few_individual < len(populationSorted):
            next_breeders += populationSorted[:self.numb_best_individual]

        for i in range(self.numb_lucky_few_individual):
            next_breeders.append(random.choice(populationSorted))
        random.shuffle(next_breeders)

        return next_breeders

    """
    Evaluate a population and retourn a sorted list
    """
    def compute_perfPopulation(self, population):
        fitness_pop = {}
        total_fitness = 0
        for individual in population:
            individual_fitness, score, n_conflict = self.fitness_evaluation(individual)
            total_fitness += individual_fitness
            fitness_pop[population.index(individual)] = ((score, n_conflict), individual_fitness)
        population_perf = self.get_proportional_fitness(fitness_pop, total_fitness)

        return population_perf

    """
    Proportional fitness
    """
    def get_proportional_fitness(self, fitness_pop, total_fitness):
        population_perf = {}
        for k, (evaluation, fitness) in fitness_pop.items():
            population_perf[k] = (fitness/total_fitness, evaluation)
        return sorted(population_perf.items(), key=operator.itemgetter(1), reverse=True)

    """
    Sum all fitness
    """
    def get_total_fitness(self, fitness_pop):
        total_fitness = 0
        for k, (conflict, fitness) in fitness_pop.items():
            total_fitness += fitness
        return total_fitness

    """
    Individual fitness evalution (solution)
    """
    def fitness_evaluation(self, individual):
        total_individual_score, n_conflicts, balance_courses = self.individual_evaluation(individual)
        if total_individual_score == 0 :
            return 1, total_individual_score, n_conflicts
        else :
            return 1/total_individual_score, total_individual_score, n_conflicts

    """
    SIndividual evaluation
    """
    def individual_evaluation(self, students_matrix):
        balance_courses = self.init_balance()
        individual_score = 0
        n_conflicts = 0
        individual_conflict = []
        for i in range(len(students_matrix)): # for each student matrix
            student_info = self.student_courses_info[i]
            for j in range(len(student_info["student_course"])): # each courses
                student_course_info = student_info["courses"][j] # get course info
                n_ex = student_info["student_course"][j].course.n_EX_Qa
                course_score, conflicts, balance_courses, slots = self.course_evaluation(students_matrix[i], n_ex, student_course_info, balance_courses)
                individual_score += course_score
                if conflicts > 0:
                    if (individual_conflict.count(student_info["student"]) == 0 and slots.count("E") > 0):
                        individual_conflict.append(student_info["student"])
                n_conflicts += conflicts
        total_balance_score = self.balance_evaluation(balance_courses)
        return individual_score+total_balance_score, n_conflicts, balance_courses

    """
    Balance evaluation
    """
    def balance_evaluation(self, balance_courses):
        score = 0
        for course_key, course_balance in balance_courses.items():
            if sum(course_balance["balance"]) > course_balance["n_students"]:
                score += 1
        return score


    """
    Course evalution
    """
    def course_evaluation(self, student_matrix, n_ex, course_info, balance_courses):
        assigned = 0
        conflict = 0
        slots = []
        if course_info["ex"]["range"] != None :
            for row in range(len(student_matrix)): # time slots
                if student_matrix[row].count(1) > 0 :
                    for col in range(course_info["ex"]["range"][0], course_info["ex"]["range"][1]): # course slots
                        if course_info["ex"]["range"][0] <= col < course_info["ex"]["range"][1]:
                            score, balance = self.ex_time_slot_checkup(student_matrix, row, col, course_info["ex"]["slots"], course_info["ex"]["range"], balance_courses[course_info["sigle"]]["balance"])
                            if balance != None :
                                balance_courses[course_info["sigle"]]["balance"] = balance # update
                            assigned += score
                            if (col == len(student_matrix[0])-1 and student_matrix[row].count(1) > 1):
                                conflict += student_matrix[row].count(1)
                                slots.append(row)
            if assigned == 0:
                conflict += 1
                slots.append("E")
        return abs(n_ex-assigned)+conflict, conflict, balance_courses, slots


    """
    EX time slot : choice available
    """
    def ex_time_slot_checkup(self, student_matrix, current_time_slot, current_course_slot, time_slot_queryset, course_range, course_balance):
        # As each course slot is related to a timeslot
        list_index = list(range(course_range[0], course_range[1]))
        index = list_index.index(current_course_slot)
        object = time_slot_queryset[index] # get the corresponding timeslot
        if current_time_slot == self.parse_time_slots(object.slot):
            if student_matrix[current_time_slot][current_course_slot] == 1:
                course_balance[list_index.index(current_course_slot)] += 1 # increment by 1
                return 1, course_balance
        return 0, None


    """
    Ranges (CM, EX) course check up
    """
    def course_range_check_up(self, matrix, slot, range_dict):
        conflict_range = 0
        n = range_dict["n"] # number of session
        my_range = range_dict["range"] # get range

        if my_range != None :
            for c in range(my_range[0], my_range[1]):
                if matrix[slot][c] == 1 :
                    conflict_range += 1
        if conflict_range > 1:
            return 1, conflict_range
        else:
            return conflict_range, conflict_range

    def get_total_conflict(self, conflict_students):
        result = 0
        for dict in conflict_students:

            result += dict["n_conflict"]
        return result

    """
    Graph : evolution of average
    """
    def evolutionAverage(self):
        evolution_conflict = []
        for population in self.historic:
            populationPerf = self.compute_perfPopulation(population)
            averageFitness = 0
            averageConflict = 0
            for k, (fitness, conflict) in populationPerf:
                averageFitness += fitness
                averageConflict += conflict[1]
            evolution_conflict.append((averageFitness/(self.size_population), averageConflict/self.size_population))
        return evolution_conflict

    """
    Graph : evolution of the best
    """
    def evolutionBestFitness(self):

        evolutionFitness = []

        for population in self.historic:
            best = self.get_bestFromGen(population)
            fitness = best["perf"][0]
            conflicts = self.get_total_conflict(best["conflicts"])
            evolutionFitness.append((fitness, conflicts))
        return evolutionFitness



    def progress_bar(self, curr, total, full_progbar):
        frac = curr/total
        filled_progbar = round(frac*full_progbar)
        print('\r', '#'*filled_progbar + '-'*(full_progbar-filled_progbar), '[{:>7.2%}]'.format(frac), end='')


    """
    Parse matrix
    """
    def parse_matrix(self, best):
        result = []
        matrix = best["student_matrix"]
        conflicts = best["conflicts"]
        for i in range(len(matrix)): # student matrix
            student_info = self.student_courses_info[i]
            student_matrix = matrix[i]
            student_result = {}
            student_result["student"] = student_info["student"]
            courses = []
            for j in range(len(student_info["student_course"])): # each courses
                student_course_info = student_info["courses"][j] # get course info
                course = {}
                course["course_model"] = student_info["student_course"][j].course
                slots = []
                for row in range(len(student_matrix)):
                    if student_matrix[row].count(1) == 1:
                        if student_course_info["ex"]["range"] != None :
                            for col in range(student_course_info["ex"]["range"][0], student_course_info["ex"]["range"][1]):
                                if student_matrix[row][col] == 1:
                                    slots.append(self.parse_matrix_slot(col, student_course_info))

                course["slots"] = slots
                courses.append(course)
            student_result["courses"] = courses
            result.append(student_result)
        return result, conflicts

    """
    Return EX_Qa_slot corresponding to the matrix slot
    """
    def parse_matrix_slot(self, col, course_info):
        my_list = list(range(course_info["ex"]["range"][0], course_info["ex"]["range"][1]))
        if col in my_list:
            return course_info["ex"]["slots"][my_list.index(col)]
        return None


