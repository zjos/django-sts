from scheduling.models import StudentCourse, Slot_EX_QA, Slot_EX_QB, Slot_CM_QA, Slot_CM_QB, PossibilityQA, PossibilityQB
from django.db.models import Q

class Possibility():

    def __init__(self, students):
        self.students = students

    """
    Get all possible lab session for each course
    """
    def save_possibilities(self):
        i = 0
        for student in self.students:
            courses = StudentCourse.objects.filter(student=student) # get student courses
            for object in courses:
                course_pos = []

                course_labs_qa = Slot_EX_QA.objects.filter(course=object.course) # get EX_QA of the course
                if course_labs_qa.count() > 0: # if course labs session exit
                    course_pos += self.possibility_lookup(student, course_labs_qa, courses, 1)

                course_labs_qb = Slot_EX_QB.objects.filter(course=object.course) # get EX_QB of the course
                if course_labs_qb.count() > 0: # if course labs session exit
                    course_pos += self.possibility_lookup(student, course_labs_qb, courses, 2)
                """
                print("====possibilities====")
                for c in courses:
                    print("course : "+ str(c.course))
                    poss = PossibilityQB.objects.filter(Q(course=c.course) & Q(student=student))
                    for p in poss:
                        print(p)
                """

            self.progress_bar(i, self.students.count(), 20)
            i += 1

    def progress_bar(self, curr, total, full_progbar):
        frac = curr/total
        filled_progbar = round(frac*full_progbar)
        print('\r', '#'*filled_progbar + '-'*(full_progbar-filled_progbar), '[{:>7.2%}]'.format(frac), end='')

    """
    Possibility lookup
    """
    def possibility_lookup(self, student, course_labs, student_courses, demi_semester):
        result = []
        if course_labs :
            other_slots = self.get_other_courses_slots(course_labs.first(), student_courses, demi_semester) # get all other course slots session
            for lab in course_labs: # for each course labs
                if demi_semester == 1:
                    overlap, object = self.overlap_checkup(student, lab, other_slots, demi_semester)
                    if not overlap:
                        PossibilityQA.objects.create(student=student,
                                         course=lab.course,
                                         ex_qa_slot=lab)
                if demi_semester == 2:
                    overlap, object = self.overlap_checkup(student, lab, other_slots, demi_semester)
                    if not overlap:
                        PossibilityQB.objects.create(student=student,
                                         course=lab.course,
                                         ex_qb_slot=lab)
        return result

    """
    get all other slots of a student
    """
    def get_other_courses_slots(self, lab, student_courses, demi_semester):
        result = []
        for object in student_courses:
            if object.course != lab.course :
                if demi_semester == 1:
                    result += list(Slot_CM_QA.objects.filter(course=object.course))
                    result += list(Slot_EX_QA.objects.filter(course=object.course))
                else :
                    result += list(Slot_CM_QB.objects.filter(course=object.course))
                    result += list(Slot_EX_QB.objects.filter(course=object.course))
        return result

    """
    Overlap checkup, return true if overlap othewise false
    """
    def overlap_checkup(self, student, lab, other_courses_slots, demi_semester):
        for object in other_courses_slots:
            if object.slot.day == lab.slot.day and object.slot.shift == lab.slot.shift: # conflict
                if isinstance(object, Slot_CM_QB) or isinstance(object, Slot_CM_QA):
                    return True, object
                else:
                    if demi_semester == 1 :
                        if PossibilityQA.objects.filter(Q(student=student) & Q(course=lab.course)).exists(): # already have a possibily
                            return True, object
                        else:
                            possibility = PossibilityQA.objects.filter(Q(student=student) & Q(course=object.course) & Q(ex_qa_slot=object))
                            if possibility: # already taken
                                possibilities = PossibilityQA.objects.filter(Q(student=student) & Q(course=object.course))
                                if possibilities.count() > 1 :
                                    possibility.delete()
                                else:
                                    return self.permutation_lookup(student, lab, object, demi_semester)
                    else :
                        if PossibilityQB.objects.filter(Q(student=student) & Q(course=lab.course)).exists(): # already have a possibily
                            return True, object
                        else:
                            possibility = PossibilityQB.objects.filter(Q(student=student) & Q(course=object.course) & Q(ex_qb_slot=object))
                            if possibility: # already taken
                                possibilities = PossibilityQB.objects.filter(Q(student=student) & Q(course=object.course))
                                if possibilities.count() > 1:
                                    possibility.delete()
                                else:
                                    return self.permutation_lookup(student, lab, object, demi_semester)

        return False, None

    """
    permutation lookup
    """
    def permutation_lookup(self, student, lab, other_lab, demi_semester):
        if demi_semester == 1:
            other_lab_sessions = Slot_EX_QA.objects.filter(course=other_lab.course)
            student_courses = StudentCourse.objects.filter(student=student)
            other_slots = self.get_other_courses_slots(other_lab_sessions.first(), student_courses, demi_semester) # get all other slots
            for session in other_lab_sessions:
                if session != other_lab:
                    overlap = False
                    for object in other_slots:
                        if object.slot.day == session.slot.day and object.slot.shift == session.slot.shift:
                            if not isinstance(object, Slot_CM_QA):
                                possibility = PossibilityQA.objects.filter(Q(student=student) & Q(course=object.course))
                                if not possibility:
                                    overlap = True
                            else:
                                overlap = True

                    if overlap == False:
                        PossibilityQA.objects.filter(Q(student=student) & Q(course=other_lab.course) & Q(ex_qa_slot=lab)).delete()
                        PossibilityQA.objects.create(student=student,
                                                     course=session.course,
                                                     ex_qa_slot=session)
                        return False, None
        else :
            other_lab_sessions = Slot_EX_QB.objects.filter(course=other_lab.course) # get other lab session
            student_courses = StudentCourse.objects.filter(student=student)  # get student course
            other_slots = self.get_other_courses_slots(other_lab_sessions.first(), student_courses, demi_semester) # get all other slots
            for session in other_lab_sessions:
                if session != other_lab:
                    overlap = False
                    for object in other_slots:
                        if object.slot.day == session.slot.day and object.slot.shift == session.slot.shift:
                            if not isinstance(object, Slot_CM_QB):
                                possibility = PossibilityQB.objects.filter(Q(student=student) & Q(course=object.course))
                                if not possibility: # not yet possibility
                                    overlap = True
                            else:
                                overlap = True
                    if overlap == False:
                        PossibilityQB.objects.filter(Q(student=student) & Q(course=other_lab.course) & Q(ex_qb_slot=lab)).delete()
                        PossibilityQB.objects.create(student=student,
                                                     course=session.course,
                                                     ex_qb_slot=session)
                        return False, None
        return True, other_lab